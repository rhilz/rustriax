﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Oxide.Core;
using Oxide.Core.Configuration;
using Oxide.Core.Plugins;
using Oxide.Core.Libraries.Covalence;
using UnityEngine;
using UnityEngine.AI;
using VLB;
using Newtonsoft.Json;

namespace Oxide.Plugins
{
    [Info("Paratroopers", "FastBurst", "2.4.0")]
    [Description("Spawns Paratroopers Event that deploys from the Cargo Plane to random area")]
    class Paratroopers : RustPlugin
    {
        [PluginReference] Plugin Kits;

        #region Vars
        private StoredData storedData;
        private DynamicConfigFile data;
        //private static NPCPlayerApex npcPlayer;

        private const string ADMIN_PERM = "paratroopers.admin";
        private const string PLAYER_PERM = "paratroopers.cancall";

        private const float calgon = 0.0066666666666667f;
        bool loaded;
        private static Paratroopers ins { get; set; }

        private static FieldInfo _memory = typeof(global::HumanNPC).GetField("myMemory", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
        private static NavMeshHit navHit;
        private static RaycastHit rayHit;

        private FrameBudgeter frameBudgeter;

        public static List<int> FltNum = new List<int>();

        private const string CARGOPLANE_PREFAB = "assets/prefabs/npc/cargo plane/cargo_plane.prefab";
        private const string SCIENTIST_PREFAB = "assets/rust.ai/agents/npcplayer/humannpc/scientist/scientistnpc.prefab";
        private const string CORPSE_PREFAB = "assets/prefabs/player/player_corpse.prefab";
        private const string CHUTE_PREFAB = "assets/prefabs/misc/parachute/parachute.prefab";
        private const string SUPPLYDROP_PREFAB = "assets/prefabs/misc/supply drop/supply_drop.prefab";
        private const string HACKABLECRATE_PREFAB = "assets/prefabs/deployable/chinooklockedcrate/codelockedhackablecrate.prefab";
        private const string BRADLEY_PREFAB = "assets/prefabs/npc/m2bradley/bradleyapc.prefab";

        private const string SMOKE_EFFECT = "assets/bundled/prefabs/fx/smoke_signal_full.prefab";
        private const string SIRENLIGHT_EFFECT = "assets/prefabs/deployable/playerioents/lights/sirenlight/electric.sirenlight.deployed.prefab";
        private const string SIRENALARM_EFFECT = "assets/prefabs/deployable/playerioents/alarms/audioalarm.prefab";
        private const string SUPPLYDROP_EFFECT = "assets/bundled/prefabs/fx/survey_explosion.prefab";

        private static LootContainer crate;
        static System.Random random = new System.Random();
        int GetRand(int l, int h) => random.Next(l, h);
        const object Null = null;
        public bool IsNight;
        CargoPlane CreatePlane() => (CargoPlane)GameManager.server.CreateEntity(CARGOPLANE_PREFAB, new Vector3(), new Quaternion(), true);

        List<SpawnPosition> spawnGrid = new List<SpawnPosition>();
        private List<MapMarkerGenericRadius> dropZoneMarkerList = new List<MapMarkerGenericRadius>();
        private List<MapMarkerGenericRadius> botMarkerList = new List<MapMarkerGenericRadius>();
        private static List<NPCPlayer> paratrooperList = new List<NPCPlayer>();
        private static List<BasePlayer> jumperList = new List<BasePlayer>();
        private static List<ulong> paratrooperCorpseIDs = new List<ulong>();
        public List<ulong> DeadNPCPlayerIds = new List<ulong>(); //to tracebackpacks
        List<LootContainer> drops = new List<LootContainer>();
        List<StorageContainer> dropsCrate = new List<StorageContainer>();
        List<BradleyAPC> dropBradley = new List<BradleyAPC>();

        private const int LAND_LAYERS = 1 << 4 | 1 << 8 | 1 << 16 | 1 << 21 | 1 << 23;
        private const int WORLD_LAYER = (1 << 0 | 1 << 16) | ~(1 << 18 | 1 << 28 | 1 << 29);

        public const string ChatIcon = "76561198035100747";
        #endregion

        #region Get Misc Functions
        Vector3 GetDropPosition()
        {
            return new Vector3(configData.Automation.JumpPositionX, configData.Automation.jumpHeight, configData.Automation.JumpPositionZ);
        }

        public int GetDefaultJumpSpread()
        {
            return configData.Paratroopers.defaultJumpSpread;
        }

        public float GetJumpHeight()
        {
            return configData.Automation.jumpHeight;
        }

        public string GetInboundMessage()
        {
            return msg("InboundJumpPositionMessageChat");
        }

        public float GetPlaneSpeed()
        {
            return configData.Automation.PlaneSpeed;
        }
        #endregion

        #region startup/close/clean-up
        private void OnServerInitialized()
        {
            //Start Loaded
            IsNight = TOD_Sky.Instance.IsNight;
            if (Kits == null)
            {
                if (configData.Notifications.ConsoleOutput)
                    Puts("WARNING: Kits Plugin is required");
            }
            else
            {
                if (configData.Notifications.ConsoleOutput)
                    Puts("Found dependecy Kits");
            }

            ins = this;
            lang.RegisterMessages(Messages, this);
            permission.RegisterPermission(ADMIN_PERM, this);
            permission.RegisterPermission(PLAYER_PERM, this);

            data = Interface.Oxide.DataFileSystem.GetFile("Paratroopers");

            foreach (KeyValuePair<string, int> kvp in configData.Cooldowns)
            {
                if (!kvp.Key.StartsWith("paratroopers."))
                    permission.RegisterPermission("paratroopers." + kvp.Key, this);
                else permission.RegisterPermission(kvp.Key, this);
            }

            if (configData.MapSettings.CreateBotMarkers)
                timer.Repeat(configData.MapSettings.BotMarkerRefreshInterval, 0, () => RefreshBotMarkers());
            //End Loaded

            frameBudgeter = new GameObject("Paratroopers.FrameBudgeter").AddComponent<FrameBudgeter>();

            ins = this;
            loaded = true;
            LoadData();

            if (configData.Automation.AutoSpawn == true)
            {
                float nextJump = Oxide.Core.Random.Range(configData.Automation.RandomJumpIntervalMinSec, configData.Automation.RandomJumpIntervalMaxSec);
                timer.Once(nextJump, () =>
                {
                    RepeatRandomJump();
                });

                if (configData.Notifications.ConsoleOutput)
                    Puts("Random Paratroopers Events will occur in {0}s", nextJump);
            }
            else
            {
                if (configData.Notifications.ConsoleOutput)
                    Puts("Random interval Paratroopers Events are turned off");
            }
            spawnGrid = CreateSpawnGrid();

            if (configData.NPC.LootType.ToLower() == "default")
            {
                Unsubscribe(nameof(OnCorpsePopulate));
                Unsubscribe(nameof(CanPopulateLoot));
            }
        }

        private void OnServerSave() => SaveData();

        private void Unload()
        {
            if (configData.Notifications.ConsoleOutput)
                Puts("Save files, destroy timers, etc");
            WipeTroopers();
            WipeBotMarkers();
            WipeDropZoneMarkers();
            ins = null;
            FltNum.Clear();

            ParatrooperPlane[] planes = UnityEngine.Object.FindObjectsOfType<ParatrooperPlane>();

            for (int i = 0; i < planes?.Length; i++)
                UnityEngine.Object.Destroy(planes[i]);

            for (int i = 0; i < NPCParatrooper.allNpcs.Count; i++)
                UnityEngine.Object.Destroy(NPCParatrooper.allNpcs[i]);

            foreach (var drop in drops)
                if (!drop.IsDestroyed)
                    drop.Kill();

            foreach (var dropc in dropsCrate)
                if (!dropc.IsDestroyed)
                    dropc.Kill();

            foreach (var dropb in dropBradley)
                if (!dropb.IsDestroyed)
                    dropb.Kill();

            configData = null;

            UnityEngine.Object.Destroy(frameBudgeter.gameObject);
        }

        public void GetFlightNumber()
        {
            FlightNumber = UnityEngine.Random.Range(3000, 9999);
            FltNum.Add(FlightNumber);
        }

        private string FormatTime(double time)
        {
            TimeSpan dateDifference = TimeSpan.FromSeconds((float)time);
            var days = dateDifference.Days;
            var hours = dateDifference.Hours;
            hours += (days * 24);
            var mins = dateDifference.Minutes;
            var secs = dateDifference.Seconds;
            return string.Format("{0:00}:{1:00}:{2:00}", hours, mins, secs);
        }
        #endregion

        #region hooks

        private object OnDeathNotice(Dictionary<string, object> data, string message)
        {
            //PrintWarning(data["VictimEntity"].ToString());

            NPCPlayer entity = data["VictimEntity"] as NPCPlayer;
            if (entity == null)
                return null;

            if (paratrooperList.Contains(entity))
            {
                paratrooperList.Remove(entity);
                return false;
            }
            return null;
        }

        private object OnCustomLootNPC(uint ID)
        {
            foreach (var npc in paratrooperList)
                if (npc?.net?.ID == ID)
                {
                    if (configData.NPC.LootType.ToLower() == "Default")
                    {
                        return null;
                    }
                    return true;
                }
            return null;
        }

        private void OnPlayerDeath(BasePlayer player, HitInfo info) => OnEntityKill(player);

        private void OnEntityKill(BaseNetworkable entity)
        {
            if (!loaded || entity == null)
                return;

            ParatrooperPlane crashComponent = entity.GetComponent<ParatrooperPlane>();
            if (crashComponent != null)
                UnityEngine.Object.Destroy(crashComponent);
        }

        private void OnPlayerDeath(ScientistNPC victim, HitInfo info)
        {
            if (!loaded)
                return;
            if (victim == null || info == null)
                return;

            ScientistNPC Paratrooper = null;

            if (victim is ScientistNPC)
            {
                Paratrooper = victim as ScientistNPC;
                BasePlayer player = info.InitiatorPlayer;
                NPCParatrooper NPCParatrooper = victim.GetComponent<NPCParatrooper>();

                if (configData.NPC.LootType.ToLower() == "inventory")
                    NPCParatrooper.PrepareInventory();

                if (paratrooperList.Contains(Paratrooper))
                {
                    if (victim == player)
                    {
                        if (configData.Notifications.NPCSuicide)
                            foreach (var Player in BasePlayer.activePlayerList)
                                if (info.damageTypes.Has(Rust.DamageType.Drowned))
                                    Player.SendConsoleCommand("chat.add", new object[] { 2, ChatIcon, string.Format(msg("ParatrooperDrowned"), victim.ToPlayer().displayName) });
                                else
                                    Player.SendConsoleCommand("chat.add", new object[] { 2, ChatIcon, string.Format(msg("ParatrooperKilledSelf"), victim.ToPlayer().displayName) });
                        paratrooperList.Remove(Paratrooper);
                        paratrooperCorpseIDs.Add(player.userID);

                        if (victim == player) return;
                    }

                    if (victim != null && player != null)
                    {
                        if (configData.Notifications.NPCDeath)
                            foreach (var Player in BasePlayer.activePlayerList)
                                Player.SendConsoleCommand("chat.add", new object[] { 2, ChatIcon, string.Format(msg("ParatrooperKilled"), player.displayName, victim.ToPlayer().displayName) });
                    }
                    return;
                }
            }
        }

        private void OnEntityDeath(BradleyAPC victim, HitInfo info)
        {
            if (!loaded)
                return;
            if (victim == null || info == null)
                return;

            BradleyAPC Bradley = null;
            Bradley = victim as BradleyAPC;

            if (victim is BradleyAPC)
            {
                if (dropBradley.Contains(Bradley))
                {
                    victim.GetOrAddComponent<APCController>();
                    BasePlayer player = info.InitiatorPlayer;
                    BaseCombatEntity player2 = info.InitiatorPlayer;

                    if (victim != null && player != null || victim != null && player2 != null)
                    {
                        if (configData.Notifications.APCDeath)
                            foreach (var Player in BasePlayer.activePlayerList)
                                Player.SendConsoleCommand("chat.add", new object[] { 2, ChatIcon, string.Format(msg("BradleyKilled"), player.displayName) });
                        dropBradley.Remove(Bradley);
                    }
                    return;
                }
            }
        }

        private object CanBradleyApcTarget(BradleyAPC bradleyApc, ScientistNPC scientist) => scientist?.GetComponent<NPCParatrooper>() != null ? (object)false : null;

        private object OnTurretTarget(NPCAutoTurret turret, ScientistNPC scientist) => scientist?.GetComponent<NPCParatrooper>() != null ? (object)true : null;

        private object CanHelicopterTarget(PatrolHelicopterAI heliAi, ScientistNPC scientist) => scientist?.GetComponent<NPCParatrooper>() != null ? (object)true : null;

        private object CanHelicopterStrafeTarget(PatrolHelicopterAI heliAi, ScientistNPC scientist) => scientist?.GetComponent<NPCParatrooper>() != null ? (object)true : null;

        private object OnNpcTarget(NPCPlayerApex npcPlayer, BaseEntity entity)
        {

            if (npcPlayer == null || entity == null)
                return null;

            bool attackerIsMine = paratrooperList.Contains(npcPlayer);

            NPCPlayer botVictim = entity as NPCPlayer;
            if (botVictim != null)
            {
                bool vicIsMine = false;
                vicIsMine = paratrooperList.Contains(botVictim);
                if (npcPlayer == botVictim)
                    return null;

                if (!attackerIsMine)
                    return null;
            }

            if (!attackerIsMine)
                return null;

            BasePlayer victim = entity as BasePlayer;
            if (victim != null)
            {
                var bData = npcPlayer.GetComponent<NPCParatrooper>();
                bData.goingHome = false;

                Item targetItem = null;

                //var active = npcPlayer?.GetActiveItem()?.GetHeldEntity() as HeldEntity;
                Item active = npcPlayer.GetActiveItem();
                if (active == null)//freshspawn catch, pre weapon draw.  
                    return null;
                List<Item> rangeToUse = new List<Item>();

                if (active == null)
                {
                    rangeToUse = bData.Weapons[0];
                    targetItem = rangeToUse[random.Next(rangeToUse.Count)];
                    foreach (var item in npcPlayer.inventory.containerBelt.itemList.Where(item => item.info.itemid == targetItem.info.itemid))
                        targetItem = item;
                }
                else if (npcPlayer.AttackTarget == null)
                {
                    ToggleAggro(npcPlayer, Convert.ToByte(!configData.NPC.Peace_Keeper), configData.NPC.Aggro_Range);
                    if (configData.NPC.AlwaysUseLights || IsNight)
                    {
                        foreach (var item in npcPlayer.inventory.containerBelt.itemList.Where(item => item.GetHeldEntity() is TorchWeapon))
                            targetItem = item;

                        if (LightEquipped(npcPlayer) != null)
                            targetItem = LightEquipped(npcPlayer);
                    }
                }
                else
                {
                    float distance = Vector3.Distance(npcPlayer.transform.position, npcPlayer.AttackTarget.transform.position);
                    if (bData.Weapons[0].Count == 1 || bData.enemyDistance == GetRange(distance))
                        targetItem = active;
                    else
                    {
                        bData.enemyDistance = GetRange(distance);
                        rangeToUse = bData.Weapons[bData.enemyDistance];
                        if (!rangeToUse.Any())
                        {
                            if (active.GetHeldEntity() as BaseMelee != null && GetRange(distance) > 1)
                                foreach (var weapon in bData.Weapons[0])
                                    if (weapon != active)
                                        targetItem = weapon;
                        }
                        else
                            targetItem = rangeToUse[random.Next(rangeToUse.Count)];
                    }
                }
                if (targetItem != null)
                    UpdateActiveItem(npcPlayer, targetItem);
                else
                    UpdateActiveItem(npcPlayer, active);

                if (configData.NPC.Peace_Keeper)
                {
                    var held = victim.GetHeldEntity();

                    var heldWeapon = held as BaseProjectile;
                    var heldFlame = held as FlameThrower;

                    if (heldWeapon != null || heldFlame != null)
                        if (!bData.AggroPlayers.Contains(victim.userID))
                            bData.AggroPlayers.Add(victim.userID);

                    if ((heldWeapon == null && heldFlame == null) || (victim.svActiveItemID == 0u))
                    {
                        if (bData.AggroPlayers.Contains(victim.userID) && !bData.coolDownPlayers.Contains(victim.userID))
                        {
                            bData.coolDownPlayers.Add(victim.userID);
                            timer.Once(configData.NPC.Peace_Keeper_Cool_Down, () =>
                            {
                                if (bData.AggroPlayers.Contains(victim.userID))
                                {
                                    bData.AggroPlayers.Remove(victim.userID);
                                    bData.coolDownPlayers.Remove(victim.userID);
                                }
                            });
                        }
                        if (!bData.AggroPlayers.Contains(victim.userID))
                            return true;
                    }
                }

                bool OnNav = npcPlayer.GetNavAgent.isOnNavMesh;

                if (OnNav)
                {
                    var distance = Vector3.Distance(npcPlayer.transform.position, victim.transform.position);
                    if (distance < 50)
                    {
                        var heightDifference = victim.transform.position.y - npcPlayer.transform.position.y;
                        if (heightDifference > 5)
                            npcPlayer.SetDestination(npcPlayer.transform.position - (Quaternion.Euler(npcPlayer.serverInput.current.aimAngles) * Vector3.forward * 2));
                    }
                }

                if (!bData.isFalling)
                {
                    var distance = Vector3.Distance(npcPlayer.transform.position, victim.transform.position);
                    if (npcPlayer.lastAttacker != victim && distance > npcPlayer.Stats.AggressionRange && distance > npcPlayer.Stats.DeaggroRange)
                        return true;

                    var held = npcPlayer.GetHeldEntity();
                    if (held == null)
                        return null;
                    if (held as BaseProjectile == null && OnNav)
                    {
                        NavMeshPath pathToEntity = new NavMeshPath();
                        npcPlayer.AiContext.AIAgent.GetNavAgent.CalculatePath(victim.ServerPosition, pathToEntity);
                        if (npcPlayer.lastAttacker != null && victim == npcPlayer.lastAttacker && pathToEntity.status == NavMeshPathStatus.PathInvalid && !bData.fleeing)
                        {
                            var heightDifference = victim.transform.position.y - npcPlayer.transform.position.y;
                            if (heightDifference > 1 && distance < 50)
                            {
                                bData.fleeing = true;
                                timer.Once(10f, () =>
                                {
                                    if (npcPlayer != null)
                                        bData.fleeing = false;
                                });
                                WipeMemory(npcPlayer);
                                return true;
                            }
                        }
                        if (pathToEntity.status != NavMeshPathStatus.PathInvalid && bData.fleeing)
                        {
                            ForceMemory(npcPlayer, victim);
                            return null;
                        }
                    }
                    ForceMemory(npcPlayer, victim);
                }

                bData.goingHome = false;

                if (npcPlayer.AttackTarget == null)
                {
                    ToggleAggro(npcPlayer, 1, npcPlayer.Stats.AggressionRange);
                    npcPlayer.AttackTarget = victim;
                    npcPlayer.lastAttacker = victim;
                }
            }

            return entity.name.Contains("agents/") ? true : Null;
        }

        private object OnNpcTarget(BaseNpc npc, NPCPlayer npcPlayer)//stops animals targeting bots
        {
            return (npcPlayer != null && paratrooperList.Contains(npcPlayer) && true) ? true : (object)null;
        }

        private object OnEntityTakeDamage(BaseCombatEntity entity, HitInfo info)
        {
            var botMelee = info?.Initiator as BaseMelee;
            bool melee = false;
            if (botMelee != null)
            {
                melee = true;
                info.Initiator = botMelee.GetOwnerPlayer();
            }
            NPCPlayerApex bot = entity as NPCPlayerApex;
            NPCParatrooper bData;

            //If victim is one of mine
            if (bot != null && paratrooperList.Contains(bot))
            {
                var attackPlayer = info?.Initiator as BasePlayer;

                if (attackPlayer != null)
                {
                    if (paratrooperList.Contains(attackPlayer))//and attacker is one of mine
                        if (attackPlayer.GetComponent<NPCParatrooper>() == entity)
                            return true; //wont attack their own

                    if (configData.NPC.Peace_Keeper && (info.Weapon is BaseMelee || info.Weapon is TorchWeapon))//prevent melee farming with peacekeeper on
                    {
                        info.damageTypes.ScaleAll(0);
                        return true;
                    }

                    if (Vector3.Distance(attackPlayer.transform.position, bot.transform.position) > bot.Stats.AggressionRange)
                    {
                        if (bot.Stats.AggressionRange < 400)
                        {
                            bot.Stats.AggressionRange += 400;
                            bot.Stats.DeaggroRange += 400;
                        }
                        ForceMemory(bot, attackPlayer);
                        timer.Repeat(1f, 20, () =>
                        {
                            if (bot != null)
                            {
                                bot.RandomMove();
                                if (bot.AttackTarget != null && bot.AttackTarget.IsVisible(bot.eyes.position, (bot.AttackTarget as BasePlayer).eyes.position, 400))
                                    Rust.Ai.HumanAttackOperator.AttackEnemy(bot.AiContext, Rust.Ai.AttackOperator.AttackType.LongRange);
                            }
                        });

                        timer.Once(20, () =>
                        {
                            if (bot == null)
                                return;
                            bot.Stats.AggressionRange = configData.NPC.Aggro_Range;
                            bot.Stats.DeaggroRange += configData.NPC.DeAggro_Range;
                        });
                    }

                    bot.AttackTarget = attackPlayer;
                    bot.lastAttacker = attackPlayer;
                    //bData.goingHome = false;
                }
            }
            NPCPlayerApex attackNPC = info?.Initiator as NPCPlayerApex;
            //if attacker is one of mine
            if (attackNPC != null && entity is BasePlayer && paratrooperList.Contains(attackNPC))
            {
                bData = attackNPC.GetComponent<NPCParatrooper>();
                float rand = GetRand(1, 101);
                float distance = Vector3.Distance(info.Initiator.transform.position, entity.transform.position);

                float newAccuracy = configData.NPC.TrooperAccuracy;
                float newDamage = configData.NPC.TrooperDamageScale / 100f;
                if (distance > 100f && bData.enemyDistance != 4) //sniper exemption 
                {
                    newAccuracy = configData.NPC.TrooperAccuracy / (distance / 100f);
                    newDamage = newDamage / (distance / 100f);
                }
                if (!melee && newAccuracy < rand)
                    return true;
                info.damageTypes.ScaleAll(newDamage);
            }
            return null;
        }


        #region IgnoreSleeper
        private object OnNpcTarget(BaseEntity npc, BasePlayer player) => CanIgnoreSleeper(player) ? true : (object)null;

        private object CanBradleyApcTarget(BradleyAPC apc, BasePlayer player) => CanIgnoreSleeper(player) ? false : (object)null;

        private bool CanIgnoreSleeper(BasePlayer player)
        {
            if (player == null || !player.userID.IsSteamId()) return false;
            if (!player.IsSleeping()) return false;
            return true;
        }
        #endregion IgnoreSleeper

        private void WipeMemory(NPCPlayerApex npc)
        {
            npc.lastDealtDamageTime = Time.time - 21;
            npc.lastAttackedTime = Time.time - 31;
            npc.AttackTarget = null;
            npc.lastAttacker = null;
            npc.SetFact(NPCPlayerApex.Facts.HasEnemy, 0, true, true);
        }

        private void ForceMemory(NPCPlayerApex npc, BasePlayer victim)
        {
            npc.SetFact(NPCPlayerApex.Facts.HasEnemy, 1, true, true);
            Vector3 vector3;
            float single, single1, single2;
            Rust.Ai.BestPlayerDirection.Evaluate(npc, victim.ServerPosition, out vector3, out single);
            Rust.Ai.BestPlayerDistance.Evaluate(npc, victim.ServerPosition, out single1, out single2);
            var info = new Rust.Ai.Memory.ExtendedInfo();
            npc.AiContext.Memory.Update(victim, victim.ServerPosition, 1, vector3, single, single1, 1, true, 1f, out info);
        }

        #region WeaponHandling
        private int GetRange(float distance)
        {
            if (distance < 2f) return 1;
            if (distance < 10f) return 2;
            if (distance < 40f) return 3;
            return 4;
        }

        private void UpdateActiveItem(NPCPlayerApex npcPlayer, Item item)
        {
            Item activeItem1 = npcPlayer.GetActiveItem();
            HeldEntity heldEntity;
            HeldEntity heldEntity1;
            if (activeItem1 != item)
            {
                npcPlayer.svActiveItemID = 0U;
                if (activeItem1 != null)
                {
                    heldEntity = activeItem1.GetHeldEntity() as HeldEntity;
                    if (heldEntity != null)
                        heldEntity.SetHeld(false);
                }
                npcPlayer.svActiveItemID = item.uid;
                npcPlayer.SendNetworkUpdate(BasePlayer.NetworkQueue.Update);
                npcPlayer.inventory.UpdatedVisibleHolsteredItems();
                npcPlayer.SendNetworkUpdate(BasePlayer.NetworkQueue.Update);
                SetRange(npcPlayer, item);
                heldEntity1 = npcPlayer.GetActiveItem()?.GetHeldEntity() as HeldEntity;
                if (heldEntity1 != null)
                    heldEntity1.SetHeld(true);
            }
            else
            {
                var lights = configData.NPC.AlwaysUseLights;
                heldEntity1 = npcPlayer.GetActiveItem()?.GetHeldEntity() as HeldEntity;
                if (heldEntity1 != null)
                    heldEntity1.SetLightsOn(lights ? true : IsNight);
                HeadLampToggle(npcPlayer, lights ? true : IsNight);
            }
        }

        private Item LightEquipped(NPCPlayerApex npcPlayer)
        {
            foreach (var item in npcPlayer.inventory.containerBelt.itemList.Where(item => item.GetHeldEntity() is BaseProjectile && item.contents != null))
                foreach (var mod in (item.contents.itemList).Where(mod => mod.GetHeldEntity() as ProjectileWeaponMod != null && mod.info.name == "flashlightmod.item"))
                    return item;
            return null;
        }

        private void SetRange(NPCPlayerApex npcPlayer, Item item)
        {
            var bData = npcPlayer.GetComponent<NPCParatrooper>();
            var weapon = npcPlayer.GetHeldEntity() as AttackEntity;
            if (bData != null && weapon != null)
                weapon.effectiveRange = bData.Weapons[1].Contains(item) ? 2 : 350;
        }

        private void HeadLampToggle(NPCPlayerApex npcPlayer, bool On)
        {
            foreach (var item in npcPlayer.inventory.containerWear.itemList)
                if (item.info.shortname.Equals("hat.miner") || item.info.shortname.Equals("hat.candle"))
                {
                    if (On)
                    {
                        if (item.IsOn()) break;
                        item.SwitchOnOff(true);
                        npcPlayer.inventory.ServerUpdate(0f);
                        break;
                    }
                    if (!item.IsOn()) break;
                    item.SwitchOnOff(false);
                    npcPlayer.inventory.ServerUpdate(0f);
                    break;
                }
        }
        #endregion

        private void OnEntityTakeDamage(ScientistNPC scientistNPC, HitInfo hitInfo)
        {
            if (!loaded)
                return;
            if (scientistNPC == null || hitInfo == null)
                return;

            NPCParatrooper NPCParatrooper = scientistNPC.GetComponent<NPCParatrooper>();
            if (NPCParatrooper != null && hitInfo.InitiatorPlayer != null)
                NPCParatrooper.OnReceivedDamage(hitInfo.InitiatorPlayer);
        }

        private object CanPopulateLoot(global::HumanNPC player, NPCPlayerCorpse corpse) => player.GetComponent<NPCParatrooper>() != null ? (object)false : null;

        private object OnCorpsePopulate(global::HumanNPC player, NPCPlayerCorpse corpse)
        {
            if (!loaded)
                return null;
            LootableCorpse lootableCorpe = corpse as LootableCorpse;

            if (paratrooperCorpseIDs.Contains(corpse.playerSteamID))
            {
                foreach (var ent in paratrooperCorpseIDs)
                {
                    if (lootableCorpe != null)
                        for (int i = 0; i < lootableCorpe.containers.Length; i++)
                            ClearContainer(lootableCorpe.containers[i]);

                    timer.Once(0.5f, () => corpse?.Kill());
                    DeadNPCPlayerIds.Add(corpse.playerSteamID);
                    paratrooperCorpseIDs.Remove(corpse.playerSteamID);
                    break;
                }
            }

            NPCParatrooper npcParachute = player?.GetComponent<NPCParatrooper>();
            if (npcParachute == null)
                return null;

            if (configData.NPC.LootType.ToLower() == "inventory")
            {
                npcParachute.MoveInventoryTo(corpse);
                return corpse;
            }

            PopulateLoot(corpse.containers[0], configData.NPC.RandomItems);
            return corpse;
        }

        private void ToggleAggro(NPCPlayerApex npcPlayer, int hostility, float distance)
        {
            var bData = npcPlayer.GetComponent<NPCParatrooper>();
            if (bData != null)
            {
                npcPlayer.Stats.AggressionRange = distance;
                npcPlayer.Stats.DeaggroRange = npcPlayer.Stats.AggressionRange + 20;
                npcPlayer.Stats.Hostility = hostility;
            }
        }

        private void OnEntitySpawned(DroppedItemContainer container)
        {
            NextTick(() =>
            {
                if (!loaded || container == null || container.IsDestroyed)
                    return;

                if (container.playerSteamID == 0) return;

                if (DeadNPCPlayerIds.Contains(container.playerSteamID))
                {
                    container.Kill();
                    DeadNPCPlayerIds.Remove(container.playerSteamID);
                    return;
                }
            });
        }

        private object CanLootPlayer(ScientistNPC victim, BasePlayer player)
        {
            if (victim?.GetComponent<NPCParatrooper>() != null && victim.IsWounded())
                return false;
            return null;
        }

        private object OnPlayerAssist(ScientistNPC victim, BasePlayer player)
        {
            if (victim?.GetComponent<NPCParatrooper>() != null)
                return false;
            return null;
        }

        private static void RunEffect(string name, BaseEntity entity = null, Vector3 position = new Vector3(), Vector3 offset = new Vector3())
        {
            if (entity != null)
                Effect.server.Run(name, entity, 0, offset, position, null, true);
            else Effect.server.Run(name, position, Vector3.up, null, true);
        }

        private string GetGridString(Vector3 position)
        {
            Vector2 adjPosition = new Vector2((World.Size / 2) + position.x, (World.Size / 2) - position.z);
            return $"{NumberToString((int)(adjPosition.x / 145))}{((int)(adjPosition.y / 145))}";
        }

        private static string NumberToString(int number)
        {
            bool a = number > 26;
            System.Char c = (System.Char)(65 + (a ? number - 26 : number));
            return a ? "A" + c : c.ToString();
        }

        private static void StripInventory(BasePlayer player)
        {
            Item[] allItems = player.inventory.AllItems();

            for (int i = allItems.Length - 1; i >= 0; i--)
            {
                Item item = allItems[i];
                item.RemoveFromContainer();
                item.Remove();
            }
        }

        private static void ClearContainer(ItemContainer container)
        {
            if (container == null || container.itemList == null)
                return;

            while (container.itemList.Count > 0)
            {
                Item item = container.itemList[0];
                item.RemoveFromContainer();
                item.Remove(0f);
            }
        }

        private static void PopulateLoot(ItemContainer container, ConfigData.LootContainer loot)
        {
            ClearContainer(container);

            int amount = UnityEngine.Random.Range(loot.Minimum, loot.Maximum);

            List<ConfigData.LootItem> list = new List<ConfigData.LootItem>(loot.Items);

            for (int i = 0; i < amount; i++)
            {
                if (list.Count == 0)
                    list = new List<ConfigData.LootItem>(loot.Items);

                ConfigData.LootItem lootItem = list.GetRandom();
                list.Remove(lootItem);

                Item item = ItemManager.CreateByName(lootItem.Name, UnityEngine.Random.Range(lootItem.Minimum, lootItem.Maximum), lootItem.Skin);
                item.MoveToContainer(container);
            }
        }

        private static object FindPointOnNavmesh(Vector3 targetPosition)
        {
            for (int i = 0; i < 10; i++)
            {
                NavMeshHit navHit;
                if (NavMesh.SamplePosition(targetPosition, out navHit, 40, -1))
                {
                    if (IsNearOrInRock(navHit.position))
                        continue;
                    return navHit.position;
                }
            }
            return null;
        }

        private static bool IsNearOrInRock(Vector3 position)
        {
            bool flag = false;
            int hits = Physics.OverlapSphereNonAlloc(position, 2f, Vis.colBuffer, WORLD_LAYER, QueryTriggerInteraction.Ignore);
            for (int i = 0; i < hits; i++)
            {
                flag = Vis.colBuffer[i].gameObject?.name.Contains("rock_") ?? false;
                Vis.colBuffer[i] = null;
            }

            if (Physics.Raycast(position, Vector3.up, out rayHit, 15f, WORLD_LAYER, QueryTriggerInteraction.Ignore))
            {
                if (rayHit.collider?.gameObject?.name.Contains("rock_") ?? false)
                    flag = true;
            }

            if (!flag)
            {
                if (Physics.Raycast(position, Vector3.down, out rayHit, 10f, WORLD_LAYER, QueryTriggerInteraction.Ignore))
                {
                    if (!(rayHit.collider is TerrainCollider))
                        return false;
                }
            }
            return flag;
        }

        #endregion

        #region Map Markers
        private void KillMapMarker(MapMarkerGenericRadius mapMarker)
        {
            if (dropZoneMarkerList.Contains(mapMarker))
                dropZoneMarkerList.Remove(mapMarker);

            if (botMarkerList.Contains(mapMarker))
                botMarkerList.Remove(mapMarker);

            mapMarker.Kill();
            mapMarker.SendUpdate();
        }

        private void CreateBotMarker(Vector3 pos)
        {
            var mapMarker = GameManager.server.CreateEntity("assets/prefabs/tools/map/genericradiusmarker.prefab", pos) as MapMarkerGenericRadius;
            mapMarker.alpha = configData.MapSettings.BotDropZoneAlpha;
            mapMarker.color1 = Color.yellow;
            mapMarker.radius = configData.MapSettings.BotDropZoneRadius;

            mapMarker.Spawn();
            mapMarker.SendUpdate();

            botMarkerList.Add(mapMarker);
        }

        private void CreateDropZoneMarker(Vector3 pos)
        {
            var mapMarker = GameManager.server.CreateEntity("assets/prefabs/tools/map/genericradiusmarker.prefab", pos) as MapMarkerGenericRadius;
            mapMarker.alpha = configData.MapSettings.DropZoneAlpha;
            mapMarker.color1 = Color.red;
            mapMarker.radius = configData.MapSettings.DropZoneRadius;

            mapMarker.Spawn();
            mapMarker.SendUpdate();

            timer.Once(configData.NPC.Lifetime + 90f, () => KillMapMarker(mapMarker));  // add time to account for drop time
            dropZoneMarkerList.Add(mapMarker);
        }

        private void RefreshBotMarkers()
        {
            WipeBotMarkers();
            foreach (ScientistNPC npcPlayer in paratrooperList)
            {
                try
                {
                    Vector3 botPos = npcPlayer.transform.position;
                    CreateBotMarker(botPos);
                }
                catch { }
            }

            foreach (ScientistNPC jumperPlayer in jumperList)
            {
                try
                {
                    Vector3 botPos = jumperPlayer.transform.position;
                    CreateBotMarker(botPos);
                }
                catch { }
            }
        }

        List<string> GetParatrooperPositions()
        {
            List<string> returnPositionList = new List<string>();

            foreach (ScientistNPC jumperPlayer in jumperList)
            {
                try
                {
                    Vector3 botPos = jumperPlayer.transform.position;
                    string gridPos = GetGridString(botPos);

                    if (!returnPositionList.Contains(gridPos))
                        returnPositionList.Add(gridPos);
                }
                catch { }
            }

            foreach (ScientistNPC npcPlayer in paratrooperList)
            {
                try
                {
                    Vector3 botPos = npcPlayer.transform.position;
                    string gridPos = GetGridString(botPos);

                    if (!returnPositionList.Contains(gridPos))
                        returnPositionList.Add(gridPos);
                }
                catch { }
            }

            return returnPositionList;
        }

        private string GetParatrooperGridLocations()
        {
            List<string> troopPositionList = GetParatrooperPositions();

            if (troopPositionList.Count == 0)
                return msg("NoParatroopers");

            string returnValue = msg("ParatroopersFound");

            foreach (string gridLoc in troopPositionList)
                returnValue = returnValue + " <color=orange>" + gridLoc + "</color>";

            return returnValue;
        }
        #endregion

        #region Wipe Elements
        private void WipeDropZoneMarkers()
        {
            foreach (MapMarkerGenericRadius mapmarker in dropZoneMarkerList)
            {
                mapmarker.Kill();
                mapmarker.SendUpdate();
            }
        }

        private void WipeBotMarkers()
        {
            foreach (MapMarkerGenericRadius mapmarker in botMarkerList)
            {
                mapmarker.Kill();
                mapmarker.SendUpdate();
            }

            botMarkerList.Clear();
        }

        private void WipeTroopers()
        {
            foreach (ScientistNPC jumperPlayer in jumperList)
                jumperPlayer.Kill();

            foreach (ScientistNPC npcPlayer in paratrooperList)
                npcPlayer.Kill();

            paratrooperCorpseIDs.Clear();
            DeadNPCPlayerIds.Clear();
            jumperList.Clear();
            paratrooperList.Clear();
            FltNum.Clear();
        }

        private void KillEvent()
        {
            if (paratrooperList != null && paratrooperList.Any() || jumperList != null && jumperList.Any())
                WipeTroopers();

            if (paratrooperList != null && paratrooperList.Any())
                WipeBotMarkers();

            if (botMarkerList != null && botMarkerList.Any())
                WipeDropZoneMarkers();

            foreach (var drop in drops)
                if (!drop.IsDestroyed)
                    drop.Kill();

            foreach (var dropc in dropsCrate)
                if (!dropc.IsDestroyed)
                    dropc.Kill();

            foreach (var dropb in dropBradley)
                if (!dropb.IsDestroyed)
                    dropb.Kill();

            FltNum.Clear();
            ParatrooperPlane[] planes = UnityEngine.Object.FindObjectsOfType<ParatrooperPlane>();

            for (int i = 0; i < planes?.Length; i++)
                UnityEngine.Object.Destroy(planes[i]);
        }
        #endregion

        #region Misc Functions
        private Vector3 SpreadPosition(Vector3 origPosition, int amtToSpread)
        {
            int spreadX = Oxide.Core.Random.Range(amtToSpread * -1, amtToSpread);
            int spreadZ = Oxide.Core.Random.Range(amtToSpread * -1, amtToSpread);

            return new Vector3((origPosition.x + spreadX), origPosition.y, (origPosition.z + spreadZ));
        }

        private void RepeatRandomJump()
        {
            // Call random jump

            if (BasePlayer.activePlayerList.Count >= configData.Automation.RequiredPlayers)

                TriggerRandomJump();
            else
                if (configData.Notifications.ConsoleOutput)
                Puts("Not enough players for event.");

            // Queue up the next one..
            float nextJump = Oxide.Core.Random.Range(configData.Automation.RandomJumpIntervalMinSec, configData.Automation.RandomJumpIntervalMaxSec);
            timer.Once(nextJump, () => RepeatRandomJump());
            if (configData.Notifications.ConsoleOutput)
                Puts("A random Paratroopers Event will occur in {0}s", nextJump);
        }

        private void TriggerRandomJump()
        {
            var plane = CreatePlane();
            //Vector3 jumpPosition = SpreadPosition(GetRandomPosition(), 0);
            Vector3 jumpPosition = plane.RandomDropPosition();
            if (configData.Notifications.ConsoleOutput)
                Puts("A random Paratroopers Event will occur near {0}", GetGridString(jumpPosition));
            CallParatroopers(jumpPosition, configData.NPC.RandomJumperCount, null, 0, true);
        }
        #endregion

        #region Grid Teleport	
        public class SpawnPosition
        {
            const float aboveGoundPosition = 2.5f;
            public Vector3 Position;
            public Vector3 GroundPosition;
            public string GridReference;

            public SpawnPosition(Vector3 position)
            {
                Position = position;
                GroundPosition = GetGroundPosition(new Vector3(position.x, 25, position.z));
            }

            public bool isPositionAboveWater()
            {
                if ((TerrainMeta.HeightMap.GetHeight(Position) - TerrainMeta.WaterMap.GetHeight(Position)) >= 0)
                    return false;
                return true;
            }

            public float WaterDepthAtPosition()
            {
                return (TerrainMeta.WaterMap.GetHeight(Position) - TerrainMeta.HeightMap.GetHeight(Position));
            }

            Vector3 GetGroundPosition(Vector3 sourcePos)
            {

                RaycastHit hitInfo;
                if (Physics.Raycast(sourcePos, Vector3.down, out hitInfo))
                    sourcePos.y = hitInfo.point.y;

                sourcePos.y = Mathf.Max(sourcePos.y, TerrainMeta.HeightMap.GetHeight(sourcePos)) + aboveGoundPosition;

                return sourcePos;
            }
        }

        List<SpawnPosition> CreateSpawnGrid()
        {
            try
            {
                List<SpawnPosition> retval = new List<SpawnPosition>();

                var worldSize = (ConVar.Server.worldsize);
                float offset = worldSize / 2;
                var gridWidth = (calgon * worldSize);
                float step = worldSize / gridWidth;
                string start = "";

                char letter = 'A';
                int number = 0;

                for (float zz = offset; zz > -offset; zz -= step)
                {
                    for (float xx = -offset; xx < offset; xx += step)
                    {
                        var sp = new SpawnPosition(new Vector3(xx, 0, zz));
                        sp.GridReference = $"{start}{letter}{number}";
                        retval.Add(sp);
                        if (letter.ToString().ToUpper() == "Z")
                        {
                            start = "A";
                            letter = 'A';
                        }
                        else
                        {
                            letter = (char)(((int)letter) + 1);
                        }
                    }
                    number++;
                    start = "";
                    letter = 'A';
                }
                return retval;
            }
            catch (Exception ex)
            {
                throw new Exception($"CreateSpawnGrid {ex.Message}");
            }
        }
        #endregion

        #region Drop Chutes
        public class CrateDropChute : MonoBehaviour
        {
            SupplyDrop crate;
            Rigidbody rigidbody;
            private BaseEntity parachute;

            private void Awake()
            {
                rigidbody = crate.GetComponent<Rigidbody>();
            }

            private void Start()
            {
                AddParachute();
            }

            private void OnDestroy()
            {
                RemoveParachute();
            }

            private void AddParachute()
            {
                if (rigidbody != null)
                {
                    rigidbody.useGravity = true;
                }

                parachute = GameManager.server.CreateEntity(CHUTE_PREFAB);
                if (parachute != null)
                {
                    parachute.SetParent(crate, false);
                    parachute.Spawn();
                    parachute.SendNetworkUpdateImmediate(true);
                }
            }

            private void RemoveParachute()
            {
                if (crate.IsValid() == true && rigidbody != null)
                    rigidbody.useGravity = true;

                if (parachute.IsValid() == true)
                {
                    parachute.Kill();
                    parachute = null;
                }
            }

            private void OnCollisionEnter(Collision collision)
            {
                Effect.server.Run(SUPPLYDROP_EFFECT, GetComponent<BaseEntity>().transform.position);
                Destroy(this);
            }

            private void FixedUpdate()
            {
                if (parachute.IsValid() == true)
                {
                    rigidbody.useGravity = true;
                    rigidbody.mass = configData.LootBoxes.LootEffects.lootMass;
                    rigidbody.drag = configData.LootBoxes.LootEffects.lootDrag * (rigidbody.mass / 5f);
                    crate.transform.position -= new Vector3(0, 10f * configData.LootBoxes.LootEffects.lootMass * Time.deltaTime, 0);
                    //crate.transform.position -= new Vector3(0, 10f * Time.deltaTime, 0);
                }
            }
        }

        private class HackCrateDropChute : MonoBehaviour
        {
            private BaseEntity crate;
            private BaseEntity parachute;
            private Rigidbody rigidbody;

            private void Awake()
            {
                crate = GetComponent<BaseEntity>();
                rigidbody = crate.GetComponent<Rigidbody>();
                rigidbody.useGravity = true;

                rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
                rigidbody.mass = configData.LootBoxes.LootEffects.lootMassCrate;
                rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
                rigidbody.angularVelocity = Vector3Ex.Range(-1.75f, 1.75f);
                rigidbody.drag = configData.LootBoxes.LootEffects.lootDragCrate * (rigidbody.mass / 5f);
                rigidbody.angularDrag = 0.2f * (rigidbody.mass / 5f);
            }

            private void Start()
            {
                AddParachute();
            }

            private void OnDestroy()
            {
                RemoveParachute();
            }

            private void AddParachute()
            {
                if (rigidbody != null)
                {
                    rigidbody.useGravity = false;
                }


                parachute = GameManager.server.CreateEntity(CHUTE_PREFAB);
                if (parachute != null)
                {
                    parachute.SetParent(crate, false);
                    parachute.Spawn();
                    parachute.SendNetworkUpdateImmediate(true);
                }
            }

            private void RemoveParachute()
            {
                if (crate.IsValid() == true && rigidbody != null)
                    rigidbody.useGravity = true;

                if (parachute.IsValid() == true)
                {
                    parachute.Kill();
                    parachute = null;
                }
            }

            private void OnCollisionEnter(Collision collision)
            {
                Destroy(this);
            }

            private void FixedUpdate()
            {
                if (parachute.IsValid() == true)
                    crate.transform.position -= new Vector3(0, 10f * Time.deltaTime, 0);
            }
        }

        private class DroppingVehicle : MonoBehaviour
        {
            private BaseEntity vehicle;
            private BaseEntity parachute;
            private Rigidbody rb;

            private void Awake()
            {
                vehicle = GetComponent<BaseEntity>();
                rb = vehicle.gameObject.GetComponent<Rigidbody>();
            }

            private void Start()
            {
                AddParachute();
            }

            private void OnDestroy()
            {
                RemoveParachute();
            }

            private void AddParachute()
            {
                if (rb != null)
                {
                    rb.useGravity = false;
                }

                parachute = GameManager.server.CreateEntity(CHUTE_PREFAB);
                if (parachute != null)
                {
                    parachute.SetParent(vehicle, "parachute_attach");
                    parachute.Spawn();
                    if (configData.LootBoxes.LootEffects.enableSmoke)
                        Effect.server.Run(SMOKE_EFFECT, parachute, 0, Vector3.zero, Vector3.zero, null, true);
                }
            }

            private void RemoveParachute()
            {
                if (vehicle.IsValid() == true && rb != null)
                {
                    rb.useGravity = true;
                }

                if (parachute.IsValid() == true)
                {
                    parachute.Kill();
                    parachute = null;
                }
            }

            private void OnCollisionEnter(Collision collision)
            {
                Destroy(this);
            }

            private void FixedUpdate()
            {
                if (parachute.IsValid() == true)
                {
                    vehicle.transform.position -= new Vector3(0, 10f * Time.deltaTime, 0);
                }
            }
        }

        #endregion

        #region Jump Event Manager
        private class JumpEventManager
        {
            public Vector3 DropPosition = new Vector3();
            public int ParatrooperCount = 0;
            public int JumpedCount = 0;
            public string ParatrooperKit = "";
            public bool ParatrooperRandomKit;
            public int ParatrooperHealth = 0;
            private BaseEntity parachute;
            private bool EnableLootDrop;
            internal ParatrooperPlane AirbornePlane { get; private set; }
            internal APCController APCBradley { get; private set; }

            private CargoPlane CreatePlane() => (CargoPlane)GameManager.server.CreateEntity(CARGOPLANE_PREFAB, new Vector3(), new Quaternion(), true);
            private bool CreatedDropZoneMarker = false;

            private void a_PositionReached(object sender, EventArgs e)
            {
                Vector3 deployPos = AirbornePlane.transform.position;
                string jumpMessage = string.Format(ins.GetInboundMessage(), ins.GetGridString(deployPos));
                foreach (int FlightNumber in FltNum)
                {
                    if (configData.Notifications.Inbound)
                        if (configData.LootBoxes.LootEffects.dropBradley)
                            SendChatMessage("InboundJumpPositionMessage2", FlightNumber, ParatrooperCount, ins.GetGridString(deployPos));
                        else
                            SendChatMessage("InboundJumpPositionMessage", FlightNumber, ParatrooperCount, ins.GetGridString(deployPos));
                }

                if (configData.Notifications.ConsoleOutput)
                    ins.Puts(jumpMessage);

                CreateJumpers();
            }

            private void CreateJumpers()
            {
                Vector3 jumpPosition = ins.SpreadPosition(AirbornePlane.transform.position, ins.GetDefaultJumpSpread());

                ServerMgr.Instance.StartCoroutine(SpawnNPCs(jumpPosition, ParatrooperKit, ParatrooperHealth, ParatrooperRandomKit));
                string ent = configData.LootBoxes.typeOfBox.ToLower();
                JumpedCount = JumpedCount + 1;

                if (JumpedCount < ParatrooperCount)
                {
                    float nextJumperDelay = UnityEngine.Random.Range(0.01f, 0.5f);
                    ins.timer.Once(nextJumperDelay, () => CreateJumpers());
                }

                if (configData.MapSettings.CreateDropZoneMarkers && CreatedDropZoneMarker == false && JumpedCount >= (ParatrooperCount / 2))
                {
                    ins.CreateDropZoneMarker(jumpPosition);
                    CreatedDropZoneMarker = true;

                    if (configData.LootBoxes.enableLootBox)
                    {
                        if (ent == "supplydrop")
                            SpawnLoot();

                        if (ent == "hackablecrate")
                            SpawnLootCrate();
                    }
                    if (configData.LootBoxes.LootEffects.dropBradley)
                        ins.timer.Once(0.05f, () => SpawnBradley());
                }
            }

            private static void CreateSirenLights(BaseEntity entity)
            {
                var SirenLight = GameManager.server.CreateEntity(SIRENLIGHT_EFFECT, default(Vector3), default(Quaternion), true);

                SirenLight.gameObject.Identity();
                SirenLight.SetParent(entity as LootContainer, "parachute_attach");
                SirenLight.Spawn();
                SirenLight.SetFlag(BaseEntity.Flags.Reserved8, true);
            }

            private static void CreateSirenAlarms(BaseEntity entity)
            {
                var SirenAlarm = GameManager.server.CreateEntity(SIRENALARM_EFFECT, default(Vector3), default(Quaternion), true);

                SirenAlarm.gameObject.Identity();
                SirenAlarm.SetParent(entity as LootContainer, "parachute_attach");
                SirenAlarm.Spawn();
                SirenAlarm.SetFlag(BaseEntity.Flags.Reserved8, true);
            }

            private void CreateDropEffects(BaseEntity entity)
            {
                if (entity == null) return;

                if (configData.LootBoxes.LootEffects.enableSmoke)
                    Effect.server.Run(SMOKE_EFFECT, entity, 0, Vector3.zero, Vector3.zero, null, true);

                if (configData.LootBoxes.LootEffects.enableSirenAlarm && configData.LootBoxes.LootEffects.enableSirenAlarmNight && TOD_Sky.Instance.IsNight)
                    CreateSirenAlarms(entity);
                else if (configData.LootBoxes.LootEffects.enableSirenAlarmNight && TOD_Sky.Instance.IsNight)
                    CreateSirenAlarms(entity);
                else if (configData.LootBoxes.LootEffects.enableSirenAlarm)
                    CreateSirenAlarms(entity);
                else
                    return;

                if (configData.LootBoxes.LootEffects.enableSirenLight && configData.LootBoxes.LootEffects.enableSirenLightNight && TOD_Sky.Instance.IsNight)
                    CreateSirenLights(entity);
                else if (configData.LootBoxes.LootEffects.enableSirenLightNight && TOD_Sky.Instance.IsNight)
                    CreateSirenLights(entity);
                else if (configData.LootBoxes.LootEffects.enableSirenLight)
                    CreateSirenLights(entity);
                else
                    return;
            }

            private IEnumerator SpawnNPCs(Vector3 position, string jumperKit, int botHealth, bool randomKit)
            {
                position = ins.SpreadPosition(AirbornePlane.transform.position + (AirbornePlane.transform.up + (AirbornePlane.transform.forward * 2f)), ins.GetDefaultJumpSpread());
                Quaternion rotation = AirbornePlane.transform.rotation;

                BasePlayer npcPlayer = GameManager.server.CreateEntity(SCIENTIST_PREFAB, position, rotation) as BasePlayer;
                npcPlayer.enableSaving = false;
                npcPlayer.Spawn();
                NPCParatrooper npcParatrooper = npcPlayer.gameObject.AddComponent<NPCParatrooper>();
                npcParatrooper.AirbornePlane = AirbornePlane;

                NPCParatrooper.ParatrooperHealth = botHealth;
                NPCParatrooper.ParatrooperKit = jumperKit;
                NPCParatrooper.ParatrooperRandomKit = randomKit;

                yield return CoroutineEx.waitForEndOfFrame;
            }

            private void SpawnLoot()
            {
                Vector3 randsphere = UnityEngine.Random.onUnitSphere;
                Vector3 entpos = (AirbornePlane.transform.position + new Vector3(0f, 1.5f, 0f)) + (randsphere * UnityEngine.Random.Range(-2f, 3f));

                LootContainer crate = GameManager.server.CreateEntity(SUPPLYDROP_PREFAB, entpos, Quaternion.LookRotation(randsphere), true) as LootContainer;
                crate.Spawn();

                ins.drops.Add(crate);

                if (configData.LootBoxes.LootEffects.enableEffects)
                    CreateDropEffects(crate);

                if (configData.LootBoxes.LootEffects.enableChute)
                    crate.GetComponent<CrateDropChute>();
                else
                {
                    crate.GetComponent<SupplyDrop>().RemoveParachute();
                    Rigidbody rigidbody;
                    rigidbody = crate.GetComponent<Rigidbody>();
                    rigidbody.useGravity = true;
                    rigidbody.isKinematic = false;
                    rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
                    rigidbody.mass = configData.LootBoxes.LootEffects.lootMass;
                    rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
                    rigidbody.drag = configData.LootBoxes.LootEffects.lootDrag;
                }

                ClearContainer(crate.inventory);
                PopulateLoot(crate.inventory, configData.LootBoxes.RandomItems);

                crate.SendNetworkUpdateImmediate();

                if (configData.LootBoxes.Lifetime > 0)
                {
                    ins.timer.Once(configData.LootBoxes.Lifetime, () =>
                    {
                        ClearContainer(crate.inventory);
                        if (crate != null)
                            crate.Kill();
                    });
                }
            }

            private void SpawnLootCrate()
            {
                Vector3 randsphere = UnityEngine.Random.onUnitSphere;
                Vector3 entpos = (AirbornePlane.transform.position + new Vector3(0f, 1.5f, 0f)) + (randsphere * UnityEngine.Random.Range(-2f, 3f));

                var crate = GameManager.server.CreateEntity(HACKABLECRATE_PREFAB, entpos, Quaternion.LookRotation(randsphere), true);
                crate.Spawn();

                if (configData.LootBoxes.LootEffects.enableEffects)
                    CreateDropEffects(crate);

                (crate as HackableLockedCrate).hackSeconds = HackableLockedCrate.requiredHackSeconds - configData.LootBoxes.LootEffects.requiredHackSeconds;
                var container = crate?.GetComponent<StorageContainer>();

                if (configData.LootBoxes.LootEffects.enableChute)
                    container.GetOrAddComponent<HackCrateDropChute>();

                ins.dropsCrate.Add(container);

                ClearContainer(container.inventory);
                PopulateLoot(container.inventory, configData.LootBoxes.RandomItems);

                crate.SendNetworkUpdateImmediate();

                if (configData.LootBoxes.Lifetime > 0)
                {
                    ins.timer.Once(configData.LootBoxes.Lifetime, () =>
                    {
                        ClearContainer(container.inventory);
                        if (crate != null)
                            crate.Kill();
                    });
                }
            }

            private void SpawnBradley()
            {
                Vector3 randsphere = UnityEngine.Random.onUnitSphere;
                Vector3 entpos = (AirbornePlane.transform.position + new Vector3(0f, 1.5f, 0f)) + (randsphere * UnityEngine.Random.Range(-2f, 3f));

                var entity = GameManager.server.CreateEntity(BRADLEY_PREFAB, entpos, Quaternion.LookRotation(randsphere), true) as BradleyAPC;
                entity.Spawn();

                if (configData.LootBoxes.LootEffects.enableChuteBradley)
                    entity.GetOrAddComponent<DroppingVehicle>();

                ins.dropBradley.Add(entity);

                entity.GetOrAddComponent<APCController>();
                entity.SendNetworkUpdateImmediate();

                if (configData.LootBoxes.LootEffects.BradleyLifetime > 0)
                {
                    ins.timer.Once(configData.LootBoxes.LootEffects.BradleyLifetime, () =>
                    {
                        if (entity != null)
                        {
                            if (configData.LootBoxes.LootEffects.BradleyGibs)
                            {
                                entity.Die(new HitInfo(entity, entity, Rust.DamageType.Explosion, 10000f));
                                if (configData.Notifications.APCSuicide)
                                    foreach (var Player in BasePlayer.activePlayerList)
                                        Player.SendConsoleCommand("chat.add", new object[] { 2, ChatIcon, string.Format(msg("BradleySelfDestruct")) });
                                ins.dropBradley.Remove(entity);
                            }
                            else
                            {
                                entity.Kill();
                                if (configData.Notifications.APCSuicide)
                                    foreach (var Player in BasePlayer.activePlayerList)
                                        Player.SendConsoleCommand("chat.add", new object[] { 2, ChatIcon, string.Format(msg("BradleyRecalled")) });
                                ins.dropBradley.Remove(entity);
                            }

                        }
                    });
                }
            }
            public JumpEventManager(Vector3 jumperPosition, int jumperCount, string jumperKit, int botHealth, bool randomKit)
            {
                DropPosition = jumperPosition;
                ParatrooperCount = jumperCount;
                ParatrooperKit = jumperKit;
                ParatrooperHealth = botHealth;
                ParatrooperRandomKit = randomKit;
            }

            // actions
            public void DoJump()
            {
                CallPlane(DropPosition);
            }

            private void CallPlane(Vector3 position)
            {
                CargoPlane entity = CreatePlane();
                entity.Spawn();

                AirbornePlane = entity.gameObject.AddComponent<ParatrooperPlane>();
                AirbornePlane.InitializeFlightPath(position);
                AirbornePlane.PositionReached += a_PositionReached;
            }
        }

        #endregion

        #region Component
        private class FrameBudgeter : MonoBehaviour
        {
            private double maxMilliseconds = 1f;
            private Stopwatch sw = Stopwatch.StartNew();
            private int lastIndex = 0;

            private void Update()
            {
                sw.Reset();
                sw.Start();

                int count = NPCParatrooper.allNpcs?.Count ?? 0;
                if (lastIndex >= count)
                    lastIndex = 0;

                for (int i = lastIndex; i < count; i++)
                {
                    if (sw.Elapsed.TotalMilliseconds > maxMilliseconds)
                    {
                        lastIndex = i;
                        return;
                    }

                    NPCParatrooper.allNpcs[i]?.DoUpdate();
                }

                lastIndex = 0;
            }
        }

        private class NPCParatrooper : MonoBehaviour
        {
            internal static List<NPCParatrooper> allNpcs = new List<NPCParatrooper>();
            internal ScientistNPC NPC { get; private set; }
            internal ParatrooperPlane AirbornePlane { get; set; }

            private Transform tr;
            private Rigidbody rb;
            private Rust.AI.SimpleAIMemory memory;
            private Vector3 roamDestination;
            private float nextSetDestinationTime = 0;
            private bool isWaitingAtDestination = false;
            private BaseEntity parachute;
            private Vector3 currentWindVector = Vector3.zero;
            public bool isFalling = false;
            private ItemContainer[] containers;
            private float woundedDuration;
            private float woundedStartTime;
            private Vector3 _initialPosition;

            public static string ParatrooperKit = "";
            public static bool ParatrooperRandomKit;
            public static int ParatrooperHealth = 0;

            public List<ulong> AggroPlayers = new List<ulong>();
            public List<ulong> coolDownPlayers = new List<ulong>();
            public List<Item>[] Weapons = { new List<Item>(), new List<Item>(), new List<Item>(), new List<Item>(), new List<Item>() };
            public int enemyDistance;
            public bool fleeing;
            public bool goingHome;
            internal Vector3 InitialPosition
            {
                get
                {
                    return _initialPosition;
                }
                set
                {
                    object success = FindPointOnNavmesh(value);
                    if (success is Vector3)
                        _initialPosition = (Vector3)success;
                    else _initialPosition = value;
                }
            }

            private float secondsSinceWoundedStarted
            {
                get
                {
                    return Time.realtimeSinceStartup - this.woundedStartTime;
                }
            }

            private void Awake()
            {
                enabled = false;

                NPC = GetComponent<ScientistNPC>();
                paratrooperList.Add(NPC);
                tr = NPC.transform;

                InitializeNPC();
                InitializeVelocity();

                InvokeHandler.Invoke(this, () =>
                {
                    allNpcs.Add(this);
                    enabled = true;
                }, 2f);
            }

            private void InitializeNPC()
            {
                NPC.CancelInvoke(NPC.EnableNavAgent);
                NPC.CancelInvoke(NPC.EquipTest);

                memory = _memory.GetValue(NPC) as Rust.AI.SimpleAIMemory;

                NPC.NavAgent.areaMask = -1;
                NPC.NavAgent.agentTypeID = -1372625422;
                NPC.NavAgent.enabled = false;

                if (ParatrooperHealth > 0)
                {
                    NPC.startHealth = ParatrooperHealth;
                    NPC.InitializeHealth(ParatrooperHealth, ParatrooperHealth);
                }

                else
                {
                    NPC.startHealth = configData.NPC.Health;
                    NPC.InitializeHealth(configData.NPC.Health, configData.NPC.Health);
                }


                if (configData.NPC.Names.Length > 0)
                    NPC.displayName = configData.NPC.Names.GetRandom();

                if (configData.NPC.Kits.Length > 0)
                {
                    StripInventory(NPC);
                    if (ParatrooperRandomKit)
                        ins.NextTick(() => ins.Kits?.Call("GiveKit", NPC, configData.NPC.Kits.GetRandom()));
                    else
                        ins.NextTick(() => ins.Kits?.Call("GiveKit", NPC, ParatrooperKit));
                }

                if (configData.NPC.Lifetime > 0)
                    NPC.Invoke(() => NPC.Die(new HitInfo(NPC, NPC, Rust.DamageType.Explosion, 1000f)), configData.NPC.Lifetime + UnityEngine.Random.Range(0, 30));

                jumperList.Add(NPC);
                ins.NextTick(() => AIThinkManager.Remove(NPC));
            }

            private void OnDestroy()
            {
                allNpcs.Remove(this);

                if (parachute != null && !parachute.IsDestroyed)
                    parachute.Kill();

                if (NPC != null && !NPC.IsDestroyed)
                    NPC.Kill();
            }

            #region Parachuting
            private void Update()
            {
                if (!isFalling)
                    return;
                else
                {
                    if (Physics.Raycast(tr.position, Vector3.down, 0.5f, LAND_LAYERS))
                    {
                        enabled = false;
                        isFalling = false;

                        rb.useGravity = false;
                        rb.isKinematic = true;
                        rb.drag = 0;

                        NPC.modelState.onground = true;
                        NPC.modelState.flying = false;

                        if (parachute != null && !parachute.IsDestroyed)
                            parachute.Kill();

                        NPC.Invoke(() => WaterCheck(NPC), 2f);

                        if (configData.NPC.WoundedChance == 100 || UnityEngine.Random.Range(0, 100) <= configData.NPC.WoundedChance)
                            StartWounded();
                        else OnParachuteLand();
                    }
                }
            }

            private void WaterCheck(ScientistNPC NPC)
            {
                if (TerrainMeta.WaterMap.GetDepth(tr.position) > 0.9f)
                {
                    NPC.Die(new HitInfo(NPC, NPC, Rust.DamageType.Drowned, 1000f));
                    return;
                }
            }

            private Vector3 DirectionTowardsCrate
            {
                get
                {
                    if (crate != null && !crate.IsDestroyed)
                        return (crate.transform.position.XZ3D() - tr.position.XZ3D()).normalized;
                    return (InitialPosition.XZ3D() - tr.position.XZ3D()).normalized;
                }
            }

            private void FixedUpdate()
            {
                if (!isFalling && rb.velocity.y < 0)
                    DeployParachute();

                if (isFalling)
                {
                    Vector3 windAtPosition = Vector3.Lerp(InitialPosition, GetWindAtCurrentPos(), AirbornePlane != null ? 0.25f : 0.75f);

                    float heightFromGround = Mathf.Max(TerrainMeta.HeightMap.GetHeight(tr.position), TerrainMeta.WaterMap.GetHeight(tr.position));
                    float force = Mathf.InverseLerp(heightFromGround + 20f, heightFromGround + 60f, tr.position.y);

                    Vector3 normalizedDir = (windAtPosition.normalized * force) * configData.Paratroopers.Wind;

                    currentWindVector = Vector3.Lerp(currentWindVector, normalizedDir, Time.fixedDeltaTime * 0.25f);

                    rb.AddForceAtPosition(normalizedDir * 0.1f, tr.position, ForceMode.Force);
                    rb.AddForce(normalizedDir * 0.9f, ForceMode.Force);

                    Quaternion rotation = Quaternion.LookRotation(rb.velocity);
                    tr.rotation = NPC.eyes.rotation = NPC.ServerRotation = rotation;
                    NPC.viewAngles = rotation.eulerAngles;

                    parachute.transform.localRotation = Quaternion.Euler(0f, rotation.eulerAngles.y, 0f);
                    parachute.SendNetworkUpdate();
                }
            }

            private void InitializeVelocity()
            {
                rb = NPC.GetComponent<Rigidbody>();
                rb.useGravity = true;
                rb.isKinematic = false;
                rb.AddForce((Vector3.up * 15) + (UnityEngine.Random.onUnitSphere.XZ3D() * 5), ForceMode.Impulse);
            }

            private void DeployParachute()
            {
                parachute = GameManager.server.CreateEntity(CHUTE_PREFAB, tr.position);
                parachute.enableSaving = false;
                parachute.Spawn();

                parachute.SetParent(NPC, false);
                parachute.transform.localPosition = Vector3.up * 6f;
                parachute.transform.localRotation = Quaternion.Euler(0f, NPC.viewAngles.y, 0f);

                rb.drag = configData.Paratroopers.Drag;

                if (configData.Paratroopers.deploySmoke)
                    Effect.server.Run(SMOKE_EFFECT, parachute, 0, Vector3.zero, Vector3.zero, null, true);

                NPC.Invoke(() =>
                {
                    NPC.modelState.onground = false;
                    NPC.modelState.flying = true;
                    NPC.SendNetworkUpdate();
                }, 1f);

                isFalling = true;
            }

            private Vector3 GetWindAtCurrentPos()
            {
                float single = tr.position.y * 6f;
                Vector3 force = new Vector3(Mathf.Sin(single * 0.0174532924f), 0f, Mathf.Cos(single * 0.0174532924f));
                return force.normalized * 1f;
            }

            private void OnParachuteLand()
            {
                object success = FindPointOnNavmesh(tr.position);
                if (success is Vector3)
                {
                    NPC.NavAgent.Warp(tr.position = NPC.ServerPosition = (Vector3)success);
                    NPC.SetNavMeshEnabled(true);
                }

                if (_initialPosition == Vector3.zero)
                    InitialPosition = NPC.ServerPosition;

                jumperList.Remove(NPC);

                NPC.EquipTest();
                roamDestination = InitialPosition;
                UpdateTargetPosition(InitialPosition);
            }
            #endregion

            #region Think
            internal void DoUpdate()
            {
                if (NPC == null || NPC.IsDestroyed || isFalling || !NPC.NavAgent.enabled || NPC.IsWounded())
                    return;
                if (NPC.isMounted)
                {
                    ClearMemory();
                    return;
                }

                NPC.IsDormant = false;

                float distanceFromInit = Vector3.Distance(tr.position, InitialPosition);
                NPC.TryThink();

                if (!NPC.HasTarget())
                {
                    if (distanceFromInit > configData.NPC.RoamDistance)
                        UpdateTargetPosition(InitialPosition);
                    else
                    {
                        if (Vector3.Distance(tr.position, roamDestination) < 5f)
                        {
                            if (!isWaitingAtDestination)
                            {
                                nextSetDestinationTime = Time.time + UnityEngine.Random.Range(4f, 6f);
                                isWaitingAtDestination = true;
                                return;
                            }

                            if (isWaitingAtDestination && Time.time > nextSetDestinationTime)
                            {
                                Vector2 random = UnityEngine.Random.insideUnitSphere.normalized;
                                roamDestination = InitialPosition + (new Vector3(random.x, 0, random.y) * configData.NPC.RoamDistance);
                                roamDestination.y = TerrainMeta.HeightMap.GetHeight(roamDestination);

                                if (NavMesh.SamplePosition(roamDestination, out navHit, 5f, NPC.NavAgent.areaMask))
                                    roamDestination = navHit.position;

                                isWaitingAtDestination = false;
                            }
                        }

                        NPC.SetDestination(roamDestination);
                        NPC.SetDesiredSpeed(global::HumanNPC.SpeedType.Walk);
                    }
                }
                else
                {
                    if (!(NPC.currentTarget is BasePlayer) || distanceFromInit > (configData.NPC.RoamDistance - 5))
                    {
                        ClearMemory();
                    }
                    else
                    {
                        if (Vector3.Distance(tr.position, NPC.currentTarget.transform.position) > NPC.sightRangeLarge)
                        {
                            RemoveFromMemory(NPC.currentTarget);
                        }
                        else
                        {
                            if (NPC.IsVisibleStanding(NPC.currentTarget as BasePlayer) && NPC.DistanceToTarget() < NPC.GetAttackEntity()?.effectiveRange * 5f && Interface.CallHook("OnNpcTarget", NPC) == null)
                            {
                                NPC.SetDesiredSpeed(global::HumanNPC.SpeedType.Walk);
                                return;
                            }

                            Vector3 attackDesination = NPC.currentTarget.transform.position;

                            if (Vector3.Distance(InitialPosition, attackDesination) > configData.NPC.RoamDistance)
                                attackDesination = Quaternion.Euler((NPC.currentTarget.transform.position - InitialPosition)) * (Vector3.forward * (configData.NPC.RoamDistance - 5));

                            if (Vector3.Distance(attackDesination, tr.position) < 2f)
                            {
                                NPC.SetDesiredSpeed(global::HumanNPC.SpeedType.Walk);
                                return;
                            }

                            NPC.SetDestination(attackDesination);
                            NPC.SetDesiredSpeed(global::HumanNPC.SpeedType.Sprint);
                            return;
                        }
                    }

                    NPC.SetDesiredSpeed(global::HumanNPC.SpeedType.Walk);
                    UpdateTargetPosition(roamDestination);
                }
            }

            private void RemoveFromMemory(BaseEntity target)
            {
                for (int i = 0; i < memory.All.Count; i++)
                {
                    if (memory.All[i].Entity != null)
                    {
                        memory.Visible.Remove(memory.All[i].Entity);
                    }
                    memory.All.RemoveAt(i);
                    i--;
                }
                NPC.currentTarget = null;
            }

            private void ClearMemory()
            {
                memory.All.Clear();
                memory.Visible.Clear();
                NPC.currentTarget = null;
            }

            public void UpdateTargetPosition(Vector3 targetPosition)
            {
                if (NPC == null || NPC.IsDestroyed || NPC.NavAgent == null)
                    return;

                if (NPC.NavAgent.isOnNavMesh)
                    NPC.SetDestination(targetPosition);

            }

            public void OnReceivedDamage(BasePlayer attacker)
            {
                if (NPC == null || NPC.IsDestroyed || NPC.NavAgent == null || NPC.isMounted)
                    return;

                if (Vector3.Distance(tr.position, attacker.transform.position) <= NPC.sightRangeLarge)
                {
                    memory.Update(attacker);

                    if (NPC.currentTarget == null || NPC.currentTarget == attacker)
                    {
                        NPC.SetDestination(attacker.transform.position);
                        NPC.currentTarget = attacker;

                        if (NPC.IsVisibleStanding(NPC.currentTarget as BasePlayer) && NPC.DistanceToTarget() < NPC.GetAttackEntity()?.effectiveRange * 5f && Interface.CallHook("OnNpcTarget", NPC) == null)
                            NPC.SetDesiredSpeed(global::HumanNPC.SpeedType.Walk);
                        else
                            NPC.SetDesiredSpeed(global::HumanNPC.SpeedType.Sprint);
                    }
                }
            }
            #endregion

            #region Wounded
            private void StartWounded()
            {
                woundedDuration = UnityEngine.Random.Range(configData.NPC.WoundedMin, configData.NPC.WoundedMax);
                woundedStartTime = Time.realtimeSinceStartup;

                jumperList.Remove(NPC);

                NPC.SetPlayerFlag(BasePlayer.PlayerFlags.Wounded, true);
                NPC.EnableServerFall(true);
                NPC.SendNetworkUpdateImmediate(false);
                InvokeHandler.Invoke(this, WoundingTick, 1f);
            }

            private void WoundingTick()
            {
                if (!NPC.IsDead())
                {
                    if (secondsSinceWoundedStarted < woundedDuration)
                        InvokeHandler.Invoke(this, WoundingTick, 1f);
                    else if (UnityEngine.Random.Range(0, 100) >= configData.NPC.RecoveryChance)
                        NPC.Die(new HitInfo(NPC, NPC, Rust.DamageType.Explosion, 1000f));
                    else
                    {
                        NPC.SetPlayerFlag(BasePlayer.PlayerFlags.Wounded, false);
                        OnParachuteLand();
                    }
                }
            }
            #endregion

            #region Loot
            internal void PrepareInventory()
            {
                ItemContainer[] source = new ItemContainer[] { NPC.inventory.containerMain, NPC.inventory.containerWear, NPC.inventory.containerBelt };

                containers = new ItemContainer[3];

                for (int i = 0; i < containers.Length; i++)
                {
                    containers[i] = new ItemContainer();
                    containers[i].ServerInitialize(null, source[i].capacity);
                    containers[i].GiveUID();
                    Item[] array = source[i].itemList.ToArray();
                    for (int j = 0; j < array.Length; j++)
                    {
                        Item item = array[j];
                        if (i == 1)
                        {
                            Item newItem = ItemManager.CreateByItemID(item.info.itemid, item.amount, item.skin);
                            if (!newItem.MoveToContainer(containers[i], -1, true))
                                newItem.Remove(0f);
                        }
                        else
                        {
                            if (!item.MoveToContainer(containers[i], -1, true))
                                item.Remove(0f);
                        }
                    }
                }
            }

            internal void MoveInventoryTo(LootableCorpse corpse)
            {
                for (int i = 0; i < containers.Length; i++)
                {
                    Item[] array = containers[i].itemList.ToArray();
                    for (int j = 0; j < array.Length; j++)
                    {
                        Item item = array[j];
                        if (!item.MoveToContainer(corpse.containers[i], -1, true))
                        {
                            item.Remove(0f);
                        }
                    }
                }

                corpse.ResetRemovalTime();
            }
            #endregion
        }
        #endregion

        #region ParatrooperPlane
        private Vector3 GetRandomPosition()
        {
            float mapSize = (TerrainMeta.Size.x / 2) - 600f;
            float randomX = UnityEngine.Random.Range(-mapSize, mapSize);
            float randomY = UnityEngine.Random.Range(-mapSize, mapSize);
            return new Vector3(randomX, 0f, randomY);
        }

        private class ParatrooperPlane : MonoBehaviour
        {
            public event EventHandler PositionReached;
            protected virtual void OnPositionReached(EventArgs e)
            {
                EventHandler handler = PositionReached;
                if (handler != null)
                {
                    handler(this, e);
                }
            }

            private CargoPlane entity;
            private Vector3 targetPos;

            private Vector3 startPos;
            private Vector3 endPos;
            private float secondsToTake;

            private float planeSpeed = ins.GetPlaneSpeed();
            private float dropDistance = 75f;

            private bool hasDropped = false;

            private void Awake()
            {
                entity = GetComponent<CargoPlane>();

                entity.dropped = true;
                enabled = false;
            }

            private void Update()
            {
                float xDistance = transform.position.x - targetPos.x;
                float zDistance = transform.position.z - targetPos.z;

                if (!hasDropped && Math.Abs(xDistance) <= dropDistance && Math.Abs(zDistance) <= dropDistance)
                {
                    hasDropped = true;
                    this.OnPositionReached(EventArgs.Empty);

                }
            }

            private void OnDestroy()
            {
                enabled = false;
                CancelInvoke();
                if (entity != null && !entity.IsDestroyed)
                    entity.Kill();
                FltNum.Clear();
            }

            public void InitializeFlightPath(Vector3 targetPos)
            {
                this.targetPos = targetPos;
                targetPos += new Vector3(UnityEngine.Random.Range(-10, 10), 0, UnityEngine.Random.Range(-10, 10));

                float size = TerrainMeta.Size.x;
                float highestPoint = ins.GetJumpHeight();

                startPos = Vector3Ex.Range(-1f, 1f);
                startPos.y = 0f;
                startPos.Normalize();
                startPos = startPos * (size * 2f);
                startPos.y = highestPoint;

                endPos = startPos * -1f;
                endPos.y = startPos.y;
                startPos = startPos + targetPos;
                endPos = endPos + targetPos;

                secondsToTake = (Vector3.Distance(startPos, endPos) / planeSpeed) * UnityEngine.Random.Range(0.95f, 1.05f);

                entity.transform.position = startPos;
                entity.transform.rotation = Quaternion.LookRotation(endPos - startPos);

                entity.startPos = startPos;
                entity.endPos = endPos;
                entity.dropPosition = targetPos;
                entity.secondsToTake = secondsToTake;

                enabled = true;
            }

            public void GetFlightData(out Vector3 startPos, out Vector3 endPos, out float secondsToTake)
            {
                startPos = this.startPos;
                endPos = this.endPos;
                secondsToTake = this.secondsToTake;
            }

            public void SetFlightData(Vector3 startPos, Vector3 endPos, Vector3 targetPos, float secondsToTake)
            {
                this.startPos = startPos;
                this.endPos = endPos;
                this.targetPos = targetPos;
                this.secondsToTake = secondsToTake;

                entity.transform.position = startPos;
                entity.transform.rotation = Quaternion.LookRotation(endPos - startPos);

                entity.startPos = startPos;
                entity.endPos = endPos;
                entity.dropPosition = targetPos;
                entity.secondsToTake = secondsToTake;

                enabled = true;
            }
        }

        #endregion

        #region Bradley ACP
        private class APCController : MonoBehaviour
        {
            protected internal BradleyAPC entity { get; private set; }
            private bool isDying = false;
            private void Awake()
            {
                entity = GetComponent<BradleyAPC>();
                entity.enabled = true;
                entity.ClearPath();
                entity.IsAtFinalDestination();
                entity.searchRange = 100f;
                entity.maxCratesToSpawn = configData.LootBoxes.LootEffects.BradleyCrates;
                entity._maxHealth = configData.LootBoxes.LootEffects.BradleyHealth;
                entity.health = configData.LootBoxes.LootEffects.BradleyHealth;
            }

            private void OnDestroy()
            {
                //if (entity != null && !entity.IsDestroyed)
                //    entity.Kill();
            }

            public void ManageDamage(HitInfo info)
            {
                if (isDying)
                    return;

                if (info.damageTypes.Total() >= entity.health)
                {
                    info.damageTypes = new Rust.DamageTypeList();
                    info.HitEntity = null;
                    info.HitMaterial = 0;
                    info.PointStart = Vector3.zero;

                    OnDeath();
                }
            }

            private void RemoveCrate(LockedByEntCrate crate)
            {
                if (crate == null || (crate?.IsDestroyed ?? true))
                {
                    return;
                }
                crate.Kill();
            }

            private void OnDeath()
            {
                isDying = true;
                Effect.server.Run(entity.explosionEffect.resourcePath, entity.transform.position, Vector3.up, null, true);

                List<ServerGib> serverGibs = ServerGib.CreateGibs(entity.servergibs.resourcePath, entity.gameObject, entity.servergibs.Get().GetComponent<ServerGib>()._gibSource, Vector3.zero, 3f);

                for (int i = 0; i < 12 - entity.maxCratesToSpawn; i++)
                {
                    BaseEntity fireBall = GameManager.server.CreateEntity(entity.fireBall.resourcePath, entity.transform.position, entity.transform.rotation, true);
                    if (fireBall)
                    {
                        Vector3 onSphere = UnityEngine.Random.onUnitSphere;
                        fireBall.transform.position = (entity.transform.position + new Vector3(0f, 1.5f, 0f)) + (onSphere * UnityEngine.Random.Range(-4f, 4f));
                        Collider collider = fireBall.GetComponent<Collider>();
                        fireBall.Spawn();
                        fireBall.SetVelocity(Vector3.zero + (onSphere * UnityEngine.Random.Range(3, 10)));
                        foreach (ServerGib serverGib in serverGibs)
                            Physics.IgnoreCollision(collider, serverGib.GetCollider(), true);
                    }
                }

                if (entity != null && !entity.IsDestroyed)
                    entity.Kill(BaseNetworkable.DestroyMode.Gib);
            }
        }
        #endregion

        #region chat/console commands
        [ChatCommand("pt")]
        private void cmdChatParatroopers(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, ADMIN_PERM))
            {
                SendReply(player, "You do not have permission to use this command");
                return;
            }

            if (args.Length == 0)
            {
                var helpmsg = new StringBuilder();
                helpmsg.Append("<size=22><color=green>Paratroopers</color></size> by: FastBurst\n");
                helpmsg.Append("<color=orange>/pt kill | cancel</color> - Cancels the Paratroopers Event\n");
                helpmsg.Append("<color=orange>/pt loc</color> - Display the location of current Paratroopers\n");
                helpmsg.Append("<color=orange>/pt count</color> - Display any active counts of Paratroopers\n");
                helpmsg.Append("<color=orange>/pt random</color> - Start Paratrooper Event at a Random Location\n");
                helpmsg.Append($"<color=orange>/pt static</color> - Start Paratrooper Event at coordinates <color=green>({configData.Automation.JumpPositionX}, {configData.Automation.JumpPositionZ})</color>\n");
                helpmsg.Append("<color=orange>/pt call x x</color> - Start Paratrooper Event at specific coordinates <color=green>(x, x)</color>\n");
                helpmsg.Append("<color=orange>/pt player playername</color> - Start Paratrooper Event at Players Location\n");
                helpmsg.Append("<color=orange>/pt player playername kitname count health(optional)</color> - Start Paratrooper Event at Players Location with a specific kit and set number of paratroopers");
                SendReply(player, helpmsg.ToString().TrimEnd());
                return;
            }

            switch (args[0].ToLower())
            {
                case "kill":
                case "cancel":
                    KillEvent();
                    PrintWarning("Cleaning up event. This may take a few seconds to cycle thru. You will see a few warnings. Nothing to worry about.");
                    SendReply(player, "All Paratroopers Events has been canceled");
                    return;
                case "loc":
                    player.ChatMessage(GetParatrooperGridLocations());
                    return;
                case "count":
                    SendReply(player, $"Paratroopers Count: {paratrooperList.Count} \nParatroopers Active Jumpers: {jumperList.Count}");
                    return;
                case "random":
                    TriggerRandomJump();
                    SendReply(player, "Paratroopers will be deployed to a random location");
                    return;
                case "static":
                    Vector3 jumpPosition = ins.SpreadPosition(GetDropPosition(), 0);
                    CallParatroopers(jumpPosition, configData.NPC.RandomJumperCount, null, 0, true);
                    SendReply(player, $"ParaTroopers will be deployed to coordinates <color=green>({configData.Automation.JumpPositionX}, {configData.Automation.JumpPositionZ})</color>");
                    return;
                case "call":
                    Vector3 position = new Vector3();
                    float x, z;
                    if (!float.TryParse(args[1], out x) || !float.TryParse(args[2], out z))
                    {
                        SendReply(player, "Invalid Coordinates");
                        return;
                    }
                    else position = new Vector3(x, 0, z);
                    SendReply(player, $"ParaTroopers will be deployed to coordinates {x} - {z}");
                    CallParatroopers(position, configData.NPC.RandomJumperCount, null, 0, true);
                    return;
                case "player":
                    Vector3 location = Vector3.zero;
                    IPlayer targetPlayer = covalence.Players.FindPlayer(args[1]);

                    if (targetPlayer != null && targetPlayer.IsConnected)
                    {
                        BasePlayer target = targetPlayer?.Object as BasePlayer;
                        if (target != null)
                        {
                            if (args.Length == 4 || args.Length == 5)
                            {
                                var cmdKits = args[2];
                                int cmdCount = Convert.ToInt32(args[3]);

                                int cmdHealth = 0;
                                if (args.Length == 5)
                                    int.TryParse(args[4], out cmdHealth);

                                if (cmdCount != null && cmdKits != null)
                                {
                                    location = target.transform.position;
                                    SendReply(player, $"<color=orange>{cmdCount} Paratroopers sent towards </color>{target.displayName}'s current position");
                                    if (configData.Notifications.ConsoleOutput)
                                        Puts($"{cmdCount} Paratroopers sent towards {target.displayName}'s current position");
                                    CallParatroopers(location, cmdCount, cmdKits, cmdHealth, false);
                                    return;
                                }
                                else
                                {
                                    SendReply(player, "Invalid syntax!");
                                    return;
                                }
                            }

                            location = target.transform.position;
                            SendReply(player, $"<color=orange>Paratroopers sent towards </color>{target.displayName}'s current position");
                            if (configData.Notifications.ConsoleOutput)
                                Puts($"Paratroopers sent towards {target.displayName}'s current position");
                            CallParatroopers(location, configData.NPC.RandomJumperCount, null, 0, true);
                            return;
                        }
                    }
                    else
                    {
                        SendReply(player, "<color=orange>Could not locate the specified player</color>");
                        return;
                    }

                    return;
                default:
                    SendReply(player, "Invalid syntax!");
                    break;
            }
        }

        [ChatCommand("paratroopers")]
        private void cmdChatParatroopersLoc(BasePlayer player, string command, string[] args)
        {
            if (args.Length == 0)
            {
                SendReply(player, GetParatrooperGridLocations());
                return;
            }

            int cooldown = -1;

            if (permission.UserHasPermission(player.UserIDString, PLAYER_PERM))
            {
                foreach (KeyValuePair<string, int> kvp in configData.Cooldowns.OrderBy(x => x.Value))
                {
                    if (permission.UserHasPermission(player.UserIDString, kvp.Key))
                    {
                        cooldown = kvp.Value;
                        break;
                    }
                }
            }

            if (player.IsAdmin)
                cooldown = 0;

            if (cooldown < 0)
                return;

            if (cooldown != 0 && storedData.IsOnCooldown(player))
            {
                SendReply(player, string.Format(msg("OnCooldown", player.UserIDString), FormatTime(storedData.GetTimeRemaining(player))));
                return;
            }

            if (args.Length > 0)
            {
                if (args.Length == 1)
                {
                    if (!permission.UserHasPermission(player.UserIDString, PLAYER_PERM))
                    {
                        SendReply(player, "You do not have permission to use this command");
                        return;
                    }

                    Vector3 location = Vector3.zero;
                    IPlayer targetPlayer = covalence.Players.FindPlayer(args[0]);
                    if (targetPlayer != null && targetPlayer.IsConnected)
                    {
                        BasePlayer target = targetPlayer?.Object as BasePlayer;
                        if (target != null)
                        {
                            location = target.transform.position;
                            SendReply(player, $"<color=orange>Paratroopers sent towards </color>{target.displayName}'s current position");
                            storedData.AddCooldown(player, cooldown);
                            CallParatroopers(location, configData.NPC.RandomJumperCount, null, 0, true);
                        }
                    }
                    else
                    {
                        SendReply(player, "<color=orange>Could not locate the specified player</color>");
                        return;
                    }
                }
            }

            if (cooldown > 0)
                storedData.AddCooldown(player, cooldown);
        }

        [ConsoleCommand("pt")]
        private void ccmdGetTroopLocations(ConsoleSystem.Arg arg)
        {
            if (arg.Connection != null)
                return;

            if (arg.Args == null || arg.Args.Length == 0)
            {
                Puts("pt kill | cancel - Cancels the Paratroopers Event");
                Puts("pt loc - Display the location of current Paratroopers");
                Puts("pt count - Display any active counts of Paratroopers");
                Puts("pt random - Start Paratrooper Event at a Random Location");
                Puts("pt static - Start Paratrooper Event at coordinates ({0}, {1})", configData.Automation.JumpPositionX, configData.Automation.JumpPositionZ);
                Puts("pt call x x - Start Paratrooper Event at specific coordinates (x, x)");
                Puts("pt player playername - Start Paratrooper Event at Players Location");
                Puts("pt player playername kitname count health(optional) - Start Paratrooper Event at Players Location with a specific kit and set number of paratroopers");
                return;
            }

            switch (arg.Args[0].ToLower())
            {
                case "kill":
                case "cancel":
                    KillEvent();
                    PrintWarning("Cleaning up event. This may take a few seconds to cycle thru. You will see a few warnings. Nothing to worry about.");
                    Puts("All Paratroopers Event has been canceled");
                    return;
                case "loc":
                    Puts(GetParatrooperGridLocations());
                    return;
                case "count":
                    Puts("paratrooperList={0}, jumperList={1}", paratrooperList.Count, jumperList.Count);
                    return;
                case "random":
                    TriggerRandomJump();
                    return;
                case "static":
                    Puts("ParaTroopers will be deployed to coordinates {0} - {1}", configData.Automation.JumpPositionX, configData.Automation.JumpPositionZ);
                    Vector3 jumpPosition = ins.SpreadPosition(GetDropPosition(), 0);
                    CallParatroopers(jumpPosition, configData.NPC.RandomJumperCount, null, 0, true);
                    return;
                case "call":
                    Vector3 position = new Vector3();
                    float x, z;
                    if (!float.TryParse(arg.Args[1], out x) || !float.TryParse(arg.Args[2], out z))
                    {
                        Puts("Invalid Coordinates");
                        return;
                    }
                    else position = new Vector3(x, 0, z);
                    Puts("ParaTroopers will be deployed to coordinates {0} - {1}", x, z);
                    CallParatroopers(position, configData.NPC.RandomJumperCount, null, 0, true);
                    return;
                case "player":
                    Vector3 location = Vector3.zero;
                    IPlayer targetPlayer = covalence.Players.FindPlayer(arg.Args[1]);
                    if (targetPlayer != null && targetPlayer.IsConnected)
                    {
                        BasePlayer target = targetPlayer?.Object as BasePlayer;
                        if (target != null)
                        {
                            if (arg.Args.Length == 4 || arg.Args.Length == 5)
                            {
                                var cmdKits = arg.Args[2];
                                int cmdCount = Convert.ToInt32(arg.Args[3]);

                                int cmdHealth = 0;

                                if (arg.Args.Length == 5)
                                    int.TryParse(arg.Args[4], out cmdHealth);

                                if (cmdCount != null && cmdKits != null)
                                {
                                    location = target.transform.position;
                                    Puts($"{cmdCount} Paratroopers sent towards {target.displayName}'s current position");
                                    CallParatroopers(location, cmdCount, cmdKits, cmdHealth, false);
                                    return;
                                }
                                else
                                {
                                    Puts("Invalid syntax!");
                                    return;
                                }
                            }

                            location = target.transform.position;
                            Puts($"Paratroopers sent towards {target.displayName}'s current position");
                            CallParatroopers(location, configData.NPC.RandomJumperCount, null, 0, true);
                            return;
                        }
                    }
                    else
                    {
                        Puts("Could not locate the specified player");
                        return;
                    }

                    return;
                default:
                    break;
            }
        }
        #endregion

        #region External calls
        private void CallParatroopers(Vector3 jumperPosition, int jumperCount, string jumperKit, int bothHealth, bool randomKit)
        {
            JumpEventManager jumpEvent = new JumpEventManager(jumperPosition, jumperCount, jumperKit, bothHealth, randomKit);
            jumpEvent.DoJump();
            GetFlightNumber();
            foreach (int FlightNumber in FltNum)
            {
                if (configData.Notifications.Inbound)
                    SendChatMessage("IncomingMessage", FlightNumber);
            }
            if (configData.Notifications.ConsoleOutput)
                Puts("A Paratroopers Events has just started.");
        }
        #endregion

        #region Config
        private static ConfigData configData;
        private class ConfigData
        {
            [JsonProperty(PropertyName = "Event Automation")]
            public AutomationOptions Automation { get; set; }
            [JsonProperty(PropertyName = "Map Marker Settings")]
            public MapSettingOptions MapSettings { get; set; }
            [JsonProperty(PropertyName = "Notification Options")]
            public NotificationOptions Notifications { get; set; }
            [JsonProperty(PropertyName = "Paratrooper Chute Options")]
            public ParatroopersOptions Paratroopers { get; set; }
            [JsonProperty(PropertyName = "NPC Paratrooper Options")]
            public NPCOptions NPC { get; set; }
            [JsonProperty(PropertyName = "Loot Container Options")]
            public Loot LootBoxes { get; set; }
            [JsonProperty(PropertyName = "Command Cooldowns Timers (permission / time in minutes)")]
            public Dictionary<string, int> Cooldowns { get; set; }

            public class AutomationOptions
            {
                [JsonProperty(PropertyName = "Automatically spawn Paratroopers Events on a timer")]
                public bool AutoSpawn { get; set; }
                [JsonProperty(PropertyName = "Auto-spawn time minimum (seconds)")]
                public float RandomJumpIntervalMinSec { get; set; }
                [JsonProperty(PropertyName = "Auto-spawn time maximum (seconds)")]
                public float RandomJumpIntervalMaxSec { get; set; }
                [JsonProperty(PropertyName = "Minimum amount of online players to trigger the event")]
                public int RequiredPlayers { get; set; }
                [JsonProperty(PropertyName = "Paratrooper Plane Jump Height")]
                public float jumpHeight { get; set; }
                [JsonProperty(PropertyName = "Paratrooper Plane incoming speed")]
                public float PlaneSpeed { get; set; }
                [JsonProperty(PropertyName = "Static Paratrooper Jump Point X Coordinate")]
                public float JumpPositionX { get; set; }
                [JsonProperty(PropertyName = "Static Paratrooper Jump Point Z Coordinate")]
                public float JumpPositionZ { get; set; }
            }

            public class MapSettingOptions
            {
                [JsonProperty(PropertyName = "Paratrooper Marker Refresh rate (seconds)")]
                public float BotMarkerRefreshInterval { get; set; }
                [JsonProperty(PropertyName = "Create Paratroopers Drop Zone Marker on map")]
                public bool CreateDropZoneMarkers { get; set; }
                [JsonProperty(PropertyName = "Paratroopers Drop Zone Radius")]
                public float DropZoneRadius { get; set; }
                [JsonProperty(PropertyName = "Paratroopers Drop Zone Alpha Shading")]
                public float DropZoneAlpha { get; set; }
                [JsonProperty(PropertyName = "Create Individual Paratrooper Landing Zone Markers on map")]
                public bool CreateBotMarkers { get; set; }
                [JsonProperty(PropertyName = "Paratroopers individual Landing Zone Radius")]
                public float BotDropZoneRadius { get; set; }
                [JsonProperty(PropertyName = "Paratroopers individual Landing Zone Alpha Shading")]
                public float BotDropZoneAlpha { get; set; }
            }

            public class NotificationOptions
            {
                [JsonProperty(PropertyName = "Show notification when Paratroopers are inbound")]
                public bool Inbound { get; set; }
                [JsonProperty(PropertyName = "Show notification when Paratroopers begin Jumping")]
                public bool JumpNotification { get; set; }
                [JsonProperty(PropertyName = "Show notification when a NPC has been killed")]
                public bool NPCDeath { get; set; }
                [JsonProperty(PropertyName = "Show notification when a NPC commited Suicide/Drowned/Despawned")]
                public bool NPCSuicide { get; set; }
                [JsonProperty(PropertyName = "Show notification when the Bradley APC has been destroyed")]
                public bool APCDeath { get; set; }
                [JsonProperty(PropertyName = "Show notification when the Bradley APC SelfDestruct/Recalled")]
                public bool APCSuicide { get; set; }
                [JsonProperty(PropertyName = "Show notifications in Console")]
                public bool ConsoleOutput { get; set; }
            }

            public class ParatroopersOptions
            {
                [JsonProperty(PropertyName = "Parachute drag force")]
                public float Drag { get; set; }
                [JsonProperty(PropertyName = "Wind force")]
                public float Wind { get; set; }
                [JsonProperty(PropertyName = "Paratrooper Spread Distance")]
                public int defaultJumpSpread { get; set; }
                [JsonProperty(PropertyName = "Deploy Paratroopers with Smoke Trail while parachuting down")]
                public bool deploySmoke { get; set; }
            }

            public class NPCOptions
            {
                [JsonProperty(PropertyName = "Amount of NPC Paratroopers to spawn")]
                public int RandomJumperCount { get; set; }
                [JsonProperty(PropertyName = "NPC Paratrooper display name (chosen at random)")]
                public string[] Names { get; set; }
                [JsonProperty(PropertyName = "NPC Paratrooper Kit to be used (chosen at random)")]
                public string[] Kits { get; set; }
                [JsonProperty(PropertyName = "NPC Health")]
                public int Health { get; set; }
                [JsonProperty(PropertyName = "NPC Paratroopers attack Accuracy (0 - 100)")]
                public int TrooperAccuracy { get; set; }
                [JsonProperty(PropertyName = "NPC Paratroopers attack Damage Scale (0 - 100)")]
                public int TrooperDamageScale { get; set; }
                [JsonProperty(PropertyName = "NPC Paratroopers Aggrovation Range")]
                public int Aggro_Range { get; set; }
                [JsonProperty(PropertyName = "NPC Paratroopers DeAggrovation Range")]
                public int DeAggro_Range { get; set; }
                [JsonProperty(PropertyName = "NPC Paratroopers use Peace Keeper mode")]
                public bool Peace_Keeper { get; set; }
                [JsonProperty(PropertyName = "NPC Paratroopers use Peace Keeper Cooldown")]
                public int Peace_Keeper_Cool_Down { get; set; }
                [JsonProperty(PropertyName = "Always use Light Sources if available")]
                public bool AlwaysUseLights { get; set; }
                [JsonProperty(PropertyName = "Chance of being wounded when landing (x / 100)")]
                public int WoundedChance { get; set; }
                [JsonProperty(PropertyName = "Wounded duration minimum time (seconds)")]
                public int WoundedMin { get; set; }
                [JsonProperty(PropertyName = "Wounded duration maximum time (seconds)")]
                public int WoundedMax { get; set; }
                [JsonProperty(PropertyName = "Chance of recovery from being wounded(x / 100)")]
                public int RecoveryChance { get; set; }
                [JsonProperty(PropertyName = "Amount of time the NPCs will be alive before suiciding (seconds, 0=disable)")]
                public int Lifetime { get; set; }
                [JsonProperty(PropertyName = "Roam distance from landing position")]
                public float RoamDistance { get; set; }
                [JsonProperty(PropertyName = "Loot type (Default, Inventory, Random)")]
                public string LootType { get; set; }
                [JsonProperty(PropertyName = "Random loot items")]
                public LootContainer RandomItems { get; set; }
            }

            public class Loot
            {
                [JsonProperty(PropertyName = "Enable Loot Box to drop with Paratroopers")]
                public bool enableLootBox { get; set; }
                [JsonProperty(PropertyName = "What type of Loot box to drop with Paratroopers (supplydrop|hackablecrate)")]
                public string typeOfBox { get; set; }
                [JsonProperty(PropertyName = "Amount of time before despawning Loot Box (seconds, 0=disable)")]
                public int Lifetime { get; set; }
                [JsonProperty(PropertyName = "Loot Box Effects Settings")]
                public LootEffectsOptions LootEffects { get; set; }
                [JsonProperty(PropertyName = "Loot container items")]
                public LootContainer RandomItems { get; set; }
            }

            public class LootEffectsOptions
            {
                [JsonProperty(PropertyName = "Enable Loot Effects")]
                public bool enableEffects { get; set; }
                [JsonProperty(PropertyName = "Enable Parachute Loot Box for either Supply Drop or HackableCrate")]
                public bool enableChute { get; set; }
                [JsonProperty(PropertyName = "Supply Drop Drag Force (without parachute)")]
                public float lootDrag { get; set; }
                [JsonProperty(PropertyName = "Supply Drop Mass Weigth (without parachute")]
                public float lootMass { get; set; }
                [JsonProperty(PropertyName = "Hackable Crate Drag Force")]
                public float lootDragCrate { get; set; }
                [JsonProperty(PropertyName = "Hackable Crate Weigth")]
                public float lootMassCrate { get; set; }
                [JsonProperty(PropertyName = "Hackable Crate time to unlock (seconds)")]
                public int requiredHackSeconds { get; set; }
                [JsonProperty(PropertyName = "Bradley ACP - Air drop a Bradley APC with Paratroopers")]
                public bool dropBradley { get; set; }
                [JsonProperty(PropertyName = "Bradley APC - Enable Parachute for Air Drop")]
                public bool enableChuteBradley { get; set; }
                [JsonProperty(PropertyName = "Bradley ACP - Amount of Crates to drop after death (0=disable)")]
                public int BradleyCrates { get; set; }
                [JsonProperty(PropertyName = "Bradley APC - Health Amount")]
                public float BradleyHealth { get; set; }
                [JsonProperty(PropertyName = "Bradley ACP - Amount of time before despawning (seconds, 0=disable)")]
                public int BradleyLifetime { get; set; }
                [JsonProperty(PropertyName = "Bradley ACP - Allow to explode instead of despawning")]
                public bool BradleyGibs { get; set; }
                [JsonProperty(PropertyName = "Enable Smoke trail Loot Box and Bradley APC on drop")]
                public bool enableSmoke { get; set; }
                [JsonProperty(PropertyName = "Enable Spinning Light trail Loot Box on drop")]
                public bool enableSirenLight { get; set; }
                [JsonProperty(PropertyName = "Enable Spinning Light trail Loot Box on drop at night only")]
                public bool enableSirenLightNight { get; set; }
                [JsonProperty(PropertyName = "Enable Siren Alarm trail Loot Box on drop")]
                public bool enableSirenAlarm { get; set; }
                [JsonProperty(PropertyName = "Enable Siren Alarm trail Loot Box on drop at night only")]
                public bool enableSirenAlarmNight { get; set; }
            }

            public class LootContainer
            {
                [JsonProperty(PropertyName = "Minimum amount of items")]
                public int Minimum { get; set; }
                [JsonProperty(PropertyName = "Maximum amount of items")]
                public int Maximum { get; set; }
                [JsonProperty(PropertyName = "Items")]
                public LootItem[] Items { get; set; }
            }

            public class LootItem
            {
                [JsonProperty(PropertyName = "Item shortname")]
                public string Name { get; set; }
                [JsonProperty(PropertyName = "Item skin ID")]
                public ulong Skin { get; set; }
                [JsonProperty(PropertyName = "Minimum amount of item")]
                public int Minimum { get; set; }
                [JsonProperty(PropertyName = "Maximum amount of item")]
                public int Maximum { get; set; }
                [JsonProperty(PropertyName = "Item Display Name")]
                public string itemDisplayName { get; set; }
            }

            public Oxide.Core.VersionNumber Version { get; set; }
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            configData = Config.ReadObject<ConfigData>();

            if (configData.Version < Version)
                UpdateConfigValues();

            Config.WriteObject(configData, true);
        }

        protected override void LoadDefaultConfig()
        {
            configData = GetBaseConfig();
        }

        private ConfigData GetBaseConfig()
        {
            return new ConfigData
            {
                Automation = new ConfigData.AutomationOptions
                {
                    AutoSpawn = true,
                    RandomJumpIntervalMinSec = 1800,
                    RandomJumpIntervalMaxSec = 5400,
                    RequiredPlayers = 1,
                    PlaneSpeed = 80f,
                    jumpHeight = 200f,
                    JumpPositionX = 0f,
                    JumpPositionZ = 0f
                },
                MapSettings = new ConfigData.MapSettingOptions
                {
                    BotMarkerRefreshInterval = 15f,
                    CreateDropZoneMarkers = true,
                    DropZoneRadius = 1f,
                    DropZoneAlpha = 0.4f,
                    CreateBotMarkers = true,
                    BotDropZoneRadius = 0.5f,
                    BotDropZoneAlpha = 0.35f
                },
                Notifications = new ConfigData.NotificationOptions
                {
                    Inbound = true,
                    JumpNotification = true,
                    NPCDeath = true,
                    NPCSuicide = true,
                    APCDeath = true,
                    APCSuicide = true,
                    ConsoleOutput = true
                },
                Paratroopers = new ConfigData.ParatroopersOptions
                {
                    Drag = 2f,
                    Wind = 10f,
                    defaultJumpSpread = 20,
                    deploySmoke = true
                },
                NPC = new ConfigData.NPCOptions
                {
                    RandomJumperCount = 10,
                    Names = new string[] { "Col. Jones", "Maj. Smith", "Capt. Willams", "Pfc. Garcia", "Sgt. Morris", "Lt. Richards", "Pvt. Sossaman" },
                    Kits = new string[] { "Soldier1", "Soldier2", "Soldier3", "Soldier5", "Soldier6", "Soldier7" },
                    Health = 250,
                    TrooperAccuracy = 40,
                    TrooperDamageScale = 50,
                    Aggro_Range = 80,
                    DeAggro_Range = 50,
                    Peace_Keeper = false,
                    Peace_Keeper_Cool_Down = 5,
                    AlwaysUseLights = true,
                    WoundedChance = 20,
                    WoundedMin = 60,
                    WoundedMax = 180,
                    RecoveryChance = 50,
                    Lifetime = 900,
                    RoamDistance = 50,
                    LootType = "Random",
                    RandomItems = new ConfigData.LootContainer
                    {
                        Minimum = 5,
                        Maximum = 12,
                        Items = new ConfigData.LootItem[]
                        {
                            new ConfigData.LootItem {Name = "apple", Skin = 0, Maximum = 6, Minimum = 2, itemDisplayName = "Apple" },
                            new ConfigData.LootItem {Name = "bearmeat.cooked", Skin = 0, Maximum = 4, Minimum = 2, itemDisplayName = "Cooked Bear Meat" },
                            new ConfigData.LootItem {Name = "blueberries", Skin = 0, Maximum = 8, Minimum = 4, itemDisplayName = "Blueberries" },
                            new ConfigData.LootItem {Name = "corn", Skin = 0, Maximum = 8, Minimum = 4, itemDisplayName = "Corn" },
                            new ConfigData.LootItem {Name = "fish.raw", Skin = 0, Maximum = 4, Minimum = 2, itemDisplayName = "Raw Fish" },
                            new ConfigData.LootItem {Name = "granolabar", Skin = 0, Maximum = 4, Minimum = 1, itemDisplayName = "Granola Bar" },
                            new ConfigData.LootItem {Name = "meat.pork.cooked", Skin = 0, Maximum = 8, Minimum = 4, itemDisplayName = "Cooked Pork" },
                            new ConfigData.LootItem {Name = "syringe.medical", Skin = 0, Maximum = 6, Minimum = 2, itemDisplayName = "Medical Syringe" },
                            new ConfigData.LootItem {Name = "largemedkit", Skin = 0, Maximum = 2, Minimum = 1, itemDisplayName = "Large Medkit" },
                            new ConfigData.LootItem {Name = "bandage", Skin = 0, Maximum = 4, Minimum = 1, itemDisplayName = "Bandage" },
                            new ConfigData.LootItem {Name = "antiradpills", Skin = 0, Maximum = 3, Minimum = 1, itemDisplayName = "Anti-Radiation Pills" },
                            new ConfigData.LootItem {Name = "ammo.rifle", Skin = 0, Maximum = 100, Minimum = 10, itemDisplayName = "5.56 Rifle Ammo" },
                            new ConfigData.LootItem {Name = "ammo.pistol", Skin = 0, Maximum = 100, Minimum = 10, itemDisplayName = "Pistol Bullet" },
                            new ConfigData.LootItem {Name = "ammo.rocket.basic", Skin = 0, Maximum = 10, Minimum = 1, itemDisplayName = "Rocket" },
                            new ConfigData.LootItem {Name = "ammo.shotgun.slug", Skin = 0, Maximum = 20, Minimum = 10, itemDisplayName = "12 Gauge Slug" },
                            new ConfigData.LootItem {Name = "pistol.m92",Skin = 0,  Maximum = 1, Minimum = 1, itemDisplayName = "M92 Pistol" },
                            new ConfigData.LootItem {Name = "rifle.l96", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "L96 Rifle" },
                            new ConfigData.LootItem {Name = "rifle.lr300", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "LR-300 Assault Rifle" },
                            new ConfigData.LootItem {Name = "rifle.ak", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "Assault Rifle" },
                            new ConfigData.LootItem {Name = "rifle.bolt", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "Bolt Action Rifle" },
                            new ConfigData.LootItem {Name = "rocket.launcher", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "Rocket Launcher" },
                            new ConfigData.LootItem {Name = "pistol.revolver", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "Revolver" }
                        }
                    }
                },
                LootBoxes = new ConfigData.Loot
                {
                    enableLootBox = true,
                    typeOfBox = "supplydrop",
                    Lifetime = 900,
                    LootEffects = new ConfigData.LootEffectsOptions
                    {
                        enableEffects = true,
                        enableChute = true,
                        lootDrag = 1.0f,
                        lootMass = 10.25f,
                        lootDragCrate = 2.0f,
                        lootMassCrate = 5.25f,
                        requiredHackSeconds = 300,
                        dropBradley = true,
                        enableChuteBradley = true,
                        BradleyCrates = 2,
                        BradleyHealth = 1500f,
                        BradleyLifetime = 900,
                        BradleyGibs = true,
                        enableSmoke = true,
                        enableSirenLight = true,
                        enableSirenLightNight = true,
                        enableSirenAlarm = true,
                        enableSirenAlarmNight = true
                    },
                    RandomItems = new ConfigData.LootContainer
                    {
                        Minimum = 7,
                        Maximum = 15,
                        Items = new ConfigData.LootItem[]
                        {
                            new ConfigData.LootItem {Name = "apple", Skin = 0, Maximum = 6, Minimum = 2, itemDisplayName = "Apple" },
                            new ConfigData.LootItem {Name = "bearmeat.cooked", Skin = 0, Maximum = 4, Minimum = 2, itemDisplayName = "Cooked Bear Meat" },
                            new ConfigData.LootItem {Name = "blueberries", Skin = 0, Maximum = 8, Minimum = 4, itemDisplayName = "Blueberries" },
                            new ConfigData.LootItem {Name = "corn", Skin = 0, Maximum = 8, Minimum = 4, itemDisplayName = "Corn" },
                            new ConfigData.LootItem {Name = "fish.raw", Skin = 0, Maximum = 4, Minimum = 2, itemDisplayName = "Raw Fish" },
                            new ConfigData.LootItem {Name = "granolabar", Skin = 0, Maximum = 4, Minimum = 1, itemDisplayName = "Granola Bar" },
                            new ConfigData.LootItem {Name = "meat.pork.cooked", Skin = 0, Maximum = 8, Minimum = 4, itemDisplayName = "Cooked Pork" },
                            new ConfigData.LootItem {Name = "syringe.medical", Skin = 0, Maximum = 6, Minimum = 2, itemDisplayName = "Medical Syringe" },
                            new ConfigData.LootItem {Name = "largemedkit", Skin = 0, Maximum = 2, Minimum = 1, itemDisplayName = "Large Medkit" },
                            new ConfigData.LootItem {Name = "bandage", Skin = 0, Maximum = 4, Minimum = 1, itemDisplayName = "Bandage" },
                            new ConfigData.LootItem {Name = "antiradpills", Skin = 0, Maximum = 3, Minimum = 1, itemDisplayName = "Anti-Radiation Pills" },
                            new ConfigData.LootItem {Name = "ammo.rifle", Skin = 0, Maximum = 100, Minimum = 10, itemDisplayName = "5.56 Rifle Ammo" },
                            new ConfigData.LootItem {Name = "ammo.pistol", Skin = 0, Maximum = 100, Minimum = 10, itemDisplayName = "Pistol Bullet" },
                            new ConfigData.LootItem {Name = "ammo.rocket.basic", Skin = 0, Maximum = 10, Minimum = 1, itemDisplayName = "Rocket" },
                            new ConfigData.LootItem {Name = "ammo.shotgun.slug", Skin = 0, Maximum = 20, Minimum = 10, itemDisplayName = "12 Gauge Slug" },
                            new ConfigData.LootItem {Name = "pistol.m92",Skin = 0,  Maximum = 1, Minimum = 1, itemDisplayName = "M92 Pistol" },
                            new ConfigData.LootItem {Name = "rifle.l96", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "L96 Rifle" },
                            new ConfigData.LootItem {Name = "rifle.lr300", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "LR-300 Assault Rifle" },
                            new ConfigData.LootItem {Name = "rifle.ak", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "Assault Rifle" },
                            new ConfigData.LootItem {Name = "rifle.bolt", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "Bolt Action Rifle" },
                            new ConfigData.LootItem {Name = "rocket.launcher", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "Rocket Launcher" },
                            new ConfigData.LootItem {Name = "pistol.revolver", Skin = 0, Maximum = 1, Minimum = 1, itemDisplayName = "Revolver" }
                        }
                    }
                },
                Cooldowns = new Dictionary<string, int>
                {
                    ["paratroopers.cancall.default"] = 120,
                    ["paratroopers.cancall.vip"] = 60
                },
                Version = Version
            };
        }

        protected override void SaveConfig() => Config.WriteObject(configData, true);

        private void UpdateConfigValues()
        {
            PrintWarning("Config update detected! Updating config values...");

            ConfigData baseConfig = GetBaseConfig();

            if (configData.Version < new Core.VersionNumber(2, 0, 0))
                configData = baseConfig;

            if (configData.Version < new Core.VersionNumber(2, 1, 2))
            {
                configData.NPC.WoundedChance = 20;
                configData.NPC.RecoveryChance = 100;
                configData.NPC.RoamDistance = 50;
                configData.NPC.LootType = "Random";
                configData.NPC.Lifetime = 900;
                configData.Notifications.NPCSuicide = true;
            }

            if (configData.Version < new Core.VersionNumber(2, 1, 4))
            {
                configData.NPC.RandomItems = baseConfig.NPC.RandomItems;
                configData.LootBoxes = baseConfig.LootBoxes;
            }

            if (configData.Version < new Core.VersionNumber(2, 1, 5))
                configData.LootBoxes.LootEffects.requiredHackSeconds = 300;

            if (configData.Version < new Core.VersionNumber(2, 2, 3))
            {
                configData.NPC.WoundedMin = 60;
                configData.NPC.WoundedMax = 180;
            }

            if (configData.Version < new Core.VersionNumber(2, 2, 5))
            {
                configData.LootBoxes.LootEffects.lootDrag = configData.LootBoxes.LootEffects.lootDrag;
                configData.LootBoxes.LootEffects.lootMass = configData.LootBoxes.LootEffects.lootMass;
                configData.LootBoxes.LootEffects.enableChute = configData.LootBoxes.LootEffects.enableChute;
            }

            if (configData.Version < new Core.VersionNumber(2, 2, 6))
            {
                configData.NPC.TrooperAccuracy = 80;
                configData.NPC.TrooperDamageScale = 50;
                configData.NPC.Aggro_Range = 80;
                configData.NPC.DeAggro_Range = 80;
                configData.NPC.Peace_Keeper = false;
                configData.NPC.Peace_Keeper_Cool_Down = 5;
                configData.NPC.AlwaysUseLights = true;
            }

            if (configData.Version < new Core.VersionNumber(2, 2, 8))
            {
                configData.LootBoxes.LootEffects.dropBradley = true;
                configData.LootBoxes.LootEffects.BradleyHealth = 1500f;
                configData.LootBoxes.LootEffects.BradleyLifetime = 900;
                configData.LootBoxes.LootEffects.BradleyGibs = true;
                configData.LootBoxes.LootEffects.enableChuteBradley = true;
                configData.Notifications.APCDeath = true;
                configData.Notifications.APCSuicide = true;
            }

            if (configData.Version < new Core.VersionNumber(2, 3, 0))
                configData.LootBoxes.LootEffects.BradleyCrates = 2;

            configData.Version = Version;
            PrintWarning("Config update completed!");
        }

        #endregion

        #region Data Management
        private void SaveData() => data.WriteObject(storedData);

        private void LoadData()
        {
            try
            {
                storedData = data.ReadObject<StoredData>();
            }
            catch
            {
                storedData = new StoredData();
            }
        }

        private class StoredData
        {
            public Dictionary<ulong, double> cooldowns = new Dictionary<ulong, double>();

            private double CurrentTime() => DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;

            public void AddCooldown(BasePlayer player, int time)
            {
                if (cooldowns.ContainsKey(player.userID))
                    cooldowns[player.userID] = CurrentTime() + (time * 60);
                else cooldowns.Add(player.userID, CurrentTime() + (time * 60));
            }

            public bool IsOnCooldown(BasePlayer player)
            {
                if (!cooldowns.ContainsKey(player.userID))
                    return false;

                if (cooldowns[player.userID] < CurrentTime())
                    return false;

                return true;
            }

            public double GetTimeRemaining(BasePlayer player)
            {
                if (!cooldowns.ContainsKey(player.userID))
                    return 0;

                return cooldowns[player.userID] - CurrentTime();
            }
        }
        #endregion

        #region Localization
        private static void SendChatMessage(string key, params object[] args)
        {
            for (int i = 0; i < BasePlayer.activePlayerList.Count; i++)
            {
                BasePlayer player = BasePlayer.activePlayerList[i];
                player.ChatMessage(args != null ? string.Format(msg(key, player.UserIDString), args) : msg(key, player.UserIDString));
            }
        }

        private static string msg(string key, string playerId = null) => ins.lang.GetMessage(key, ins, playerId);

        private Dictionary<string, string> Messages = new Dictionary<string, string>
        {
            ["IncomingMessage"] = "<color=green>[Paratroopers]</color> <color=red>ALERT WARNING</color>: Be advised, that <color=#C4FF00>ZTL Flight {0}</color> is currently inbound and will deploy a group of <color=#0099CC>Cobalt Paratroopers</color> to the island. Their mission is to find any surviors and eliminate them.</color>",
            ["InboundJumpPositionMessage"] = "<color=green>[Paratroopers]</color> <color=red>ALERT WARNING</color>: Be advised, <color=#C4FF00>ZTL Flight {0}</color> has begun deploying <color=#0099CC>{1} Cobalt Paratroopers</color> over the area near map grid marker <color=orange>{2}</color>. Use extreme caution, these soldiers are very dangerous and their intent is to kill you!",
            ["InboundJumpPositionMessage2"] = "<color=green>[Paratroopers]</color> <color=red>ALERT WARNING</color>: Be advised, <color=#C4FF00>ZTL Flight {0}</color> has begun deploying <color=#0099CC>{1} Cobalt Paratroopers</color> and a <color=#0099CC>Elite Class Bradley</color> over the area near map grid marker <color=orange>{2}</color>. Use extreme caution, these soldiers are very dangerous and their intent is to kill you!",
            ["InboundJumpPositionMessageChat"] = "Paratroopers have deployed near map grid marker {0}",
            ["NoParatroopers"] = "No paratroopers found",
            ["FoundParatroopers"] = "Paratroopers found in the following grid locations:",
            ["OnCooldown"] = "You must wait another <color=orange>{0}</color> before you can call for more Paratroopers",
            ["ParatrooperKilled"] = "<size=12><color=#0099CC>{0} finished off Paratrooper {1} at one of the Landing Zones.</color></size>",
            ["ParatrooperKilledSelf"] = "<size=12><color=#0099CC>Paratrooper {0} decided they had enough and took the easy way out at one of the Landing Zones.</color></size>",
            ["ParatrooperDrowned"] = "<size=12><color=#0099CC>Paratrooper {0} found out they couldn't swim with all the attack gear and is now lost to Davey Jones Locker.</color></size>",
            ["BradleyKilled"] = "<size=12><color=#0099CC>{0} just completely annihialted the Elite Class Bradley APC near one of the Paratrooper Drop Zones!</color></size>",
            ["BradleySelfDestruct"] = "<size=12><color=#0099CC>A massive explosion just went off in a Drop Zone! The Elite Class Bradley APC self-destructed!</color></size>",
            ["BradleyRecalled"] = "<size=12><color=#0099CC>The Elite Class Bradley APC was just recalled from one of the Drop Zones!</color></size>"
        };

        private int FlightNumber;
        #endregion

    }
}